<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

/**
 * A configuração de base do WordPress
 *
 * Este ficheiro define os seguintes parâmetros: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, e ABSPATH. Pode obter mais informação
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} no Codex. As definições de MySQL são-lhe fornecidas pelo seu serviço de alojamento.
 *
 * Este ficheiro contém as seguintes configurações:
 *
 * * Configurações de  MySQL
 * * Chaves secretas
 * * Prefixo das tabelas da base de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Definições de MySQL - obtenha estes dados do seu serviço de alojamento** //
/** O nome da base de dados do WordPress */
define( 'DB_NAME', 'zi_colegio' );

/** O nome do utilizador de MySQL */
define( 'DB_USER', 'root' );

/** A password do utilizador de MySQL  */
define( 'DB_PASSWORD', '' );

/** O nome do serviddor de  MySQL  */
define( 'DB_HOST', 'localhost' );

/** O "Database Charset" a usar na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O "Database Collate type". Se tem dúvidas não mude. */
define('DB_COLLATE', '');


define( 'WP_HOME', 'http://192.168.1.93/colegiosfxavier' );
define( 'WP_SITEURL', 'http://192.168.1.93/colegiosfxavier' );
/**#@+
 * Chaves únicas de autenticação.
 *
 * Mude para frases únicas e diferentes!
 * Pode gerar frases automáticamente em {@link https://api.wordpress.org/secret-key/1.1/salt/ Serviço de chaves secretas de WordPress.org}
 * Pode mudar estes valores em qualquer altura para invalidar todos os cookies existentes o que terá como resultado obrigar todos os utilizadores a voltarem a fazer login
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'mVQ*!?+BXxw04aag:Y`M~a,K/ bq?W!..M]WvdV=9Y_+ :JNMOE!F?fSUF1HDTy_' );
define( 'SECURE_AUTH_KEY',  'Kij-Zs{;~2W0,_RIBz+>=n`p5nb~/q/moug3k?R<wlw^5Ll!qnM_|p.1BtO#$jrq' );
define( 'LOGGED_IN_KEY',    'P.UABo$E8S9 kOoL(dp+pGgj$i[KH HUZv`m;%lPWR,yAWJ!ENOD?lr]RdxH,n%G' );
define( 'NONCE_KEY',        'x;m0o8r1|9Jb}1jnX0)FKg8LHV}<oH>*Rm=p8aj|b#xHf%7O;Dcp{:XB+0,)T8SD' );
define( 'AUTH_SALT',        '8!ouHb?I79<@zoL=XH;jn_&)d@J*`j|q{UKluN#-{d9_qGNr<X$ fqC]GJFTh<}.' );
define( 'SECURE_AUTH_SALT', 'GLl.vjxB0}]Ggiw*wGx>wf-;v2?bwzJI2Mx5:HHAOI/)=AKH1x3@fq6b}6zHGxSF' );
define( 'LOGGED_IN_SALT',   '^DVe(^z8X7(mMq-1prX2>oAnJ:@~Fp3wv0GdINZC#sL4#@@xJ&DaZDY}&]b:!SDK' );
define( 'NONCE_SALT',       'o0<6UXwc*tRxfTn{XyC&1,_?{]7L_+6XBDbWtN9+XaFM~Kj.rRpWlR/j)7v{rf=d' );

/**#@-*/

/**
 * Prefixo das tabelas de WordPress.
 *
 * Pode suportar múltiplas instalações numa só base de dados, ao dar a cada
 * instalação um prefixo único. Só algarismos, letras e underscores, por favor!
 */
$table_prefix = 'wp_';

/**
 * Para developers: WordPress em modo debugging.
 *
 * Mude isto para true para mostrar avisos enquanto estiver a testar.
 * É vivamente recomendado aos autores de temas e plugins usarem WP_DEBUG
 * no seu ambiente de desenvolvimento.
 *
 * Para mais informações sobre outras constantes que pode usar para debugging,
 * visite o Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* E é tudo. Pare de editar! */

/** Caminho absoluto para a pasta do WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Define as variáveis do WordPress e ficheiros a incluir. */
require_once(ABSPATH . 'wp-settings.php');
