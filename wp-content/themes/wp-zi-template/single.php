﻿<?php get_header(); ?>

  <div class="container-fluid newsSinglePage p-0">
    <h1 class="newsPageTitle">Notícias</h1>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
      $id = get_the_ID();
    ?>

    <div class="container-fluid newsHeader p-0" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
      <div class="container">
        <div class="headerTitles">
          <h1 class="newsTitle"><?php the_title(); ?></h1>
          <h3 class="newsSubtitle"><?= get_field('subtitulo')?></h3>
        </div>
        <?php
        $category = get_the_category();
        $firstCategory = $category[0]->cat_name;
        if($firstCategory == "Destaque"){
          echo '<div class="newsSpotlight">
            <span><i class="fas fa-star"></i></span>
            <p>Notícias em destaque</p>
          </div>';
        }?>

      </div>
    </div>
  	<div class="container newsContent">
      <div class="newsNavigation">
        <div class="arrowNavigation">
          <?php
            $next_post_link_url = get_permalink( get_adjacent_post(false,'',false)->ID );
            $prev_post_link_url = get_permalink( get_adjacent_post(false,'',true)->ID );
            $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            echo '
            <p class="arrowIndication">Clique para ver mais</p>';
            if ($next_post_link_url !== $actual_link){
              echo '
              <div class="arrowLeft">
                <a href="'.$next_post_link_url.'" title="Ver Notícia anterior"><span><i class="fas fa-arrow-left"></i></span></a>
              </div>';
            }
            if ($prev_post_link_url !== $actual_link){
              echo '
              <div class="arrowRight">
                <a href="'.$prev_post_link_url.'" title="Ver Notícia a seguir"><span><i class="fas fa-arrow-right"></i></span></a>
              </div>';
            }
          ?>
        </div>
        <div class="dotsNavigation">
          <?php
          $args = array( 'numberposts' => '6',
          'post_status' => 'publish',
          'order' => 'DESC' );
          $recent_posts = wp_get_recent_posts($args);
          echo '<ul>';
          foreach( $recent_posts as $recent ) {
            $cptitle = $recent["post_title"];
            $ptitle = $post->post_title;
            echo '<li class="newsDot '; if( $cptitle == $ptitle){echo 'active';} echo'"><a href="' . get_permalink( $recent["ID"] ) . '" title="Ver ' . esc_attr( $recent["post_title"] ) . '" ><i class="fas fa-circle"></i></a></li> ';
          }
          wp_reset_query();
          echo '</ul>';
          ?>
        </div>
      </div>
      <div class="newsText"><?php the_content(); ?></div>
  		<?php endwhile; else: ?>
  		<h1 class="title">Ups...</h1>
  		<p>... pedimos desculpa mas nenhum <em>post</em> foi encontrado!</p>
  		<?php endif; ?>
  		<p align="center"><?php posts_nav_link(); ?></p>
  	</div>
    <div class="container-fluid previousNews">
      <?php $currentN = $id;?>
        <?php
        $i = '0';
        $args = array(
        'post_type' =>  'post',
        'posts_per_page' => -1,
        'order_by' => 'DESC');
        $postslist = get_posts( $args );
        foreach ($postslist as $post) :  setup_postdata($post);
          $previd = get_the_ID();
            if ($previd < $currentN){
              if ($i == "0"){
                echo '<h2 class="prevnewsTitle">Notícias Anteriores</h2>';
              }
              ?>
              <div class="prevNews" style="background-image: url('<?php the_post_thumbnail_url(); ?>')">
    						<div class="container">
    							<div class="newsDate"><span><i class="fas fa-calendar-check"></i></span><p><?= get_the_date( 'd F Y' ); ?></p></div>
                  <div class="newsTitles">
                    <div class="newsTitles_Wrapper">
                      <h2 class="newsTitle"><?= $post->post_title ?></h2>
                      <h3 class="newsSubtitle"><?= get_field('subtitulo')?></h3>
                    </div>
                  </div>
                  <p class="newsRead"><a href="<?php the_permalink();?>">Ler Notícia</a></p>
    						</div>
    					</div>
              <?php
              $i++;
            }
        ?>
        <?php endforeach; ?>
    </div>
</div>


<?php get_footer(); ?>
