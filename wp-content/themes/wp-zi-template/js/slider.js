/*        var tag = document.createElement('script');
        tag.id = 'iframe-demo';
        tag.src = 'https://www.youtube.com/iframe_api';
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
*/
        var player;
        function onYouTubeIframeAPIReady() {
            var elems1 = document.getElementsByClassName('yt-player');
            for(var i = 0; i < elems1.length; i++) {
                
                player = new YT.Player(elems1[i], {
                    events: {
                        //'onReady': onPlayerReady,
                        'onStateChange': onPlayerStateChange
                    }
                });
            }
        }
        function onPlayerReady(event) {
            
        }
        function handleVideo(playerStatus) {
            if (playerStatus == -1) {
                // unstarted
                $('#main-slider').slick('slickPause');
            } else if (playerStatus == 0) {
                // ended
                $('#main-slider').slick('slickPlay');
                $('.navmarcas_out').stop().animate({width:'160px',height:'96px'});
                $('.exmarcas').css({"transform": "rotate(360deg)"});
                
            } else if (playerStatus == 1) {
                // playing = green                
                $('#main-slider').slick('slickPause');  
                $('.navmarcas_out').stop().animate({width:'160px',height:'96px'});
                $('.exmarcas').css({"transform": "rotate(360deg)"});
            } else if (playerStatus == 2) {
                // paused = red
                $('#main-slider').slick('slickPlay');
                $('.navmarcas_out').stop().animate({width:'160px',height:'96px'});
                $('.exmarcas').css({"transform": "rotate(360deg)"});
            } else if (playerStatus == 3) {
                // buffering = purple
            } else if (playerStatus == 5) {
                // video cued
            }
        }
        function onPlayerStateChange(event) {
            handleVideo(event.data);
        }
        
        $(function() {
            $('#main-slider').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: false,
                autoplaySpeed: 3000,
                pauseOnFocus: false,
                pauseOnHover: false,
                prevArrow: $('.bt_prev'),
                nextArrow: $('.bt_next'),
                fade: true,
                dots: false,
                infinite: true,
                adaptiveHeight: false,
                arrows: false,
                  responsive: [
                    {
                      breakpoint: 1024,
                      settings: {
                            arrows: true
                      }
                    }]
          
            });
            
            $('#main-slider').on('init', function(slick) {
                console.log("initialized")
            });
        /*    
            $("#main-slider").on("afterChange", function (){       
                if ($('.slick-current').find('.marca_agua').data('url'))
                    $('#agualink').attr('href',$('.slick-current').find('.marca_agua').data('url'));
                    
                //console.log('TESTE: ' + $('.slick-current').find('.marca_agua').data('url'));
            });
            */
            
            $('.setmarca').hover(function(e){
                var slide=$(this).data('marca');                
                 $('#main-slider').slick('slickGoTo', slide);
            });

        });
        
        $('#main-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
            $('.yt-player').each(function(){
                this.contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*')
            });
        });