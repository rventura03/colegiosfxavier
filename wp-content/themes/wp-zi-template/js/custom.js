objectFitImages();
$(window).scroll(function(e) {
  //console.log($("body").find(".slick-lightbox").length);

  if ($(document).scrollTop() > 50) {
    $('nav').addClass('shrink');
  } else {
    $('nav').removeClass('shrink');
  }
});

function toggleText(){
    $('.textToggle').slideToggle('fast', 'linear');
}

function toggleBottomMenu(){
  $('.bottomMenu').slideToggle('fast', 'linear');
}

function openContactform(){
  if ($(window).width() < 992) {
    $('#contactIcon').show();
    $('#contactContent').hide();
    $('#contactForm').slideToggle('fast', 'linear');
    $('#newsletterIcon').show();
    $('#newsletterContent').hide();
    $('#newsletterForm').hide();
  }else{
    if ($('#contactForm').css('display') == 'block'){
      $('#contactIcon').show();
      $('#contactContent').show();
      $('#contactForm').hide();
      $('#newsletterIcon').show();
      $('#newsletterContent').show();
      $('#newsletterForm').hide();
    }else{
      $('#contactIcon').show();
      $('#contactContent').hide();
      $('#contactForm').slideToggle('fast', 'linear');
      $('#newsletterIcon').hide();
      $('#newsletterContent').hide();
      $('#newsletterForm').hide();
    }
  }
}
function openNewsletterform(){
  if ($(window).width() < 992) {
    $('#contactIcon').show();
    $('#contactContent').hide();
    $('#contactForm').hide();
    $('#newsletterIcon').show();
    $('#newsletterContent').hide();
    $('#newsletterForm').slideToggle('fast', 'linear');
  }else{
    if ($('#newsletterForm').css('display') == 'block'){
      $('#contactIcon').show();
      $('#contactContent').show();
      $('#contactForm').hide();
      $('#newsletterIcon').show();
      $('#newsletterContent').show();
      $('#newsletterForm').hide();
    }else{
      $('#contactIcon').hide();
      $('#contactContent').hide();
      $('#contactForm').hide();
      $('#newsletterIcon').show();
      $('#newsletterContent').hide();
      $('#newsletterForm').slideToggle('fast', 'linear');
    }
  }
}
function closeContacts(){
  $('#contactIcon').show();
  $('#contactContent').show();
  $('#contactForm').hide();
  $('#newsletterIcon').show();
  $('#newsletterContent').show();
  $('#newsletterForm').hide();
}
$(window).resize(function(){
	closeContacts();
});
