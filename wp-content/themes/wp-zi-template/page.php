﻿<?php get_header(); ?>

	<div class="container">		
		
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		
		<h1 class="single-title"><?php the_title(); ?></h1>
		
		<?php the_content(); ?>

		<?php endwhile; else: ?>
		
		<h1 class="title">Ups...</h1>
		
		<p>... pedimos desculpa mas nenhum <em>post</em> foi encontrado!</p>
		
		<?php endif; ?>
		
		<p align="center"><?php posts_nav_link(); ?></p>
		
	</div>	


	
<?php get_footer(); ?>