<?php get_header();?>

<section class="main">
	<div class="container calendarioEscolar">
		<h1>Calendário Escolar <span><?=the_field('schoolYear');?></span></h1>
		<div class="contentCalendarioescolar">
			<div class="seasonCalendarioescolar">
				<div class="row m-0 season1st">
					<div class="col-12 col-lg-3 seasonTitle"><p>1º Período</p></div>
					<?php
					if( have_rows('1periodo') ): //parent group field
						while( have_rows('1periodo') ): the_row();
							$inicio1 = get_sub_field('1p_inicio');
							$termo1 = get_sub_field('1p_termo');
							$interrupcao1 = get_sub_field('1p_interrupcao');?>
						<?php endwhile; ?>
					<?php endif; ?>
					<div class="col-12 col-md-4 col-lg-3 p-sm-0 seasonBegin">
						<div class="seasonHead"><p class="m-0">Início</p></div>
						<div class="seasonContent"><p class="m-0"><?=$inicio1?></p></div>
					</div>
					<div class="col-12 col-md-4 col-lg-3 p-sm-0 seasonEnd">
						<div class="seasonHead"><p class="m-0">Termo</p></div>
						<div class="seasonContent"><p class="m-0"><?=$termo1?></p></div>
					</div>
					<div class="col-12 col-md-4 col-lg-3 p-sm-0 seasonInterruption">
						<div class="seasonHead"><p class="m-0">Interrupção</p></div>
						<div class="seasonContent"><p class="m-0"><?=$interrupcao1?></p></div>
					</div>
				</div>
				<div class="row m-0 season2nd">
					<div class="col-12 col-lg-3 seasonTitle"><p>2º Período</p></div>
					<?php
					if( have_rows('2periodo') ): //parent group field
						while( have_rows('2periodo') ): the_row();
							$inicio2 = get_sub_field('2p_inicio');
							$termo2 = get_sub_field('2p_termo');
							$interrupcao2 = get_sub_field('2p_interrupcao');?>
						<?php endwhile; ?>
					<?php endif; ?>
					<div class="col-12 col-md-4 col-lg-3 p-sm-0 seasonBegin">
						<div class="seasonHead"><p class="m-0">Início</p></div>
						<div class="seasonContent"><p class="m-0"><?=$inicio2?></p></div>
					</div>
					<div class="col-12 col-md-4 col-lg-3 p-sm-0 seasonEnd">
						<div class="seasonHead"><p class="m-0">Termo</p></div>
						<div class="seasonContent"><p class="m-0"><?=$termo2?></p></div>
					</div>
					<div class="col-12 col-md-4 col-lg-3 p-sm-0 seasonInterruption">
						<div class="seasonHead"><p class="m-0">Interrupção</p></div>
						<div class="seasonContent"><p class="m-0"><?=$interrupcao2?></p></div>
					</div>
				</div>
				<div class="row m-0 season3rd">
					<div class="col-12 col-lg-3 seasonTitle"><p>3º Período</p></div>
					<?php
					if( have_rows('3periodo') ): //parent group field
						while( have_rows('3periodo') ): the_row();
							$inicio3 = get_sub_field('3p_inicio');
							$termo31C = get_sub_field('3p_termo1c');
							$termo32C = get_sub_field('3p_termo2c');?>
						<?php endwhile; ?>
					<?php endif; ?>
					<div class="col-12 col-md-4 col-lg-3 p-sm-0 seasonBegin">
						<div class="seasonHead"><p class="m-0">Início</p></div>
						<div class="seasonContent"><p class="m-0"><?=$inicio3?></p></div>
					</div>
					<div class="col-12 col-md-8 col-lg-6 p-sm-0 seasonEnd">
						<div class="seasonHead"><p class="m-0">Termo</p></div>
						<div class="seasonContent1"><p class="m-0"><?=$termo31C?> (1º Ciclo)</p></div>
						<div class="seasonContent2"><p class="m-0"><?=$termo32C?> (2º Ciclo)</p></div>
					</div>
				</div>
			</div>

			<p class="infoField"><?=the_field('notas');?></p>
		</div>
		<div class="activitiesCalendar">
			<div class="row activitiescalendarContent m-0">
				<div class="col-12 col-lg-6 titleActivitiescalendar"><p>Descarregue o ficheiro PDF aqui</p></div>
				<div class="col-12 col-lg-6 buttonActivitiescalendar">
					<a href="<?=the_field('fileActivitiesplan');?>">
						<i class="fas fa-download"></i>
						<div class="button">
							Calendário de Actividades
							<span><?=the_field('schoolYear');?></span>
						</div>
					</a>
			</div>
		</div>
	</div>
</section>

<?php get_footer();?>


<script>
jQuery(document).ready(function(){
	jQuery('.scroller').scrollbar();
});
</script>
