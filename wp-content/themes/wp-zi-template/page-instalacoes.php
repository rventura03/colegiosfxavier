<?php get_header();?>

<section class="main">
	<div class="container-fluid p-0 instalacoes">
		<h2 class="galleryTitle">As nossas Instalações</h2>
		<div class="gal">
			<?php if( have_rows('instalacoes') ):
				$i = 1;
				?>
				<?php while( have_rows('instalacoes') ): the_row();?>

					<a class="<?php if($i > 9)echo"hidden";?>" href="<?php the_sub_field('image'); ?>" data-lightbox="roadtrip" title="Ver Imagem"><span class="showImage"><i class="fas fa-search-plus"></i></span><img src="<?php the_sub_field('image'); ?>" alt="Instalações"></a>
				<?php 
					$i++;
				endwhile; ?>
			<?php endif; ?>
	 	</div>
		<?php
			if($i > 9):
		?>	<div class="loadGal">
				<button class="btn loadMore">Carregar Mais</button>
			</div>
		<?php
			endif;
		?>
		<div class="container galleryText">
			<?=the_field("textContent")?>
		</div>
		<div class="locationMap">
			<h2 class="galleryTitle">Mapa</h2>
			<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1577.4729777882637!2d-25.675054!3d37.7444121!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xb432acf586b443d%3A0x25e1de3e07cecd55!2sCol%C3%A9gio%20de%20S%C3%A3o%20Francisco%20Xavier!5e0!3m2!1spt-PT!2spt!4v1579521955334!5m2!1spt-PT!2spt" width="100%" height="800px" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
		</div>
	</div>

</section>


<script>
    lightbox.option({
      'resizeDuration': 200,
      'wrapAround': false,
			'showImageNumberLabel' : false,
			'albumLabel' : "",
			'maxWidth' : 1000,
			'maxHeight' : 650,
			'fitImagesInViewport' : true,
			'positionFromTop' : 100,
			'disableScrolling' : true
    })
	$('.loadMore').click(function (e){
		e.preventDefault();
		if ($('a.hidden').css("display") === "none"){
			$('a.hidden').fadeIn("slow", function(){
				$(this).css("display", "inline-block");
			});
		}else	
			$('a.hidden').fadeOut("slow", function(){
				$(this).css("display", "none");
				$(window).scrollTop();
			});
	});
</script>

<?php get_footer();?>
