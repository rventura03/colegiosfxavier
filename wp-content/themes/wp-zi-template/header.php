<!DOCTYPE html>
<html <?php language_attributes() ?>>
  <head>
    <title><?php bloginfo('name'); ?> - <?php bloginfo('description'); ?></title>
    <link rel="SHORTCUT ICON" href="<?php bloginfo('template_directory'); ?>/favicon.ico" />
    <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
    <meta http-equiv="X-UA-Compatible" content="IE=10">
    <meta name="description" content="<?php bloginfo('description'); ?>" />
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/bootstrap.css"/>
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/slick.css"/>
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/slick-lightbox.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/hamburgers.css"/>
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/all.min.css"/>
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/lightbox.css"/>
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/imgal.min.css"/>
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/style.css"/>

    <script src="<?php bloginfo('template_directory'); ?>/js/jquery.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/node_modules/object-fit-images/dist/ofi.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/popper.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/slick.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/slick-lightbox.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/lightbox.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/all.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/jquery.scrollbar.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/imgal.js"></script>
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
    <header>
      <div class="top-navbar">
        <div class="container">
          <div id="mobileExpand" class="mobileExpand d-lg-none"><i class="fas fa-chevron-down"></i></div>
          <div id="topnavContent" class="row">
            <div class="col-md-12 col-lg-auto topnavItem">
              <p><i class="fas fa-phone-alt"></i> <a href="tel:296205200" title="phone">296 205 200</a></p>
            </div>
            <div class="col-md-12 col-lg-auto topnavItem">
              <p><i class="fas fa-envelope"></i> <a href="mailto:info@colegiosfxavier.com" title="Email">info@colegiosfxavier.com</a></p>
            </div>
            <div class="col-md-12 col-lg-auto topnavItem">
              <p><span>Horário Semanal</span> <i class="far fa-clock"></i> Segunda a Sexta 07h30 - 19h00</p>
            </div>
            <div class="col-md-12 col-lg topnavItem">
              <p><a href="https://www.facebook.com/Col%C3%A9gio-De-S%C3%A3o-Francisco-Xavier-383576428467465/" target="_blank" title="Facebook"><i class="fab fa-facebook"></i></a></p>
            </div>
          </div>
        </div>
      </div>

      <nav class="navbar navbar-expand-lg navbar-light navbar-fixed-top" role="navigation">
          <div class="container">
            <a class="navbar-brand" href="<?=home_url();?>" title="Logo">
              <img class="site-logo" src="<?=content_url();?>/uploads/2020/02/csfx-logogold.png" alt="csfxLogo"/>
            </a>
            <div class="collapse navbar-collapse" id="navbarNav">
              <?php
                wp_nav_menu();
              ?>
              <div class="searchForm">
                <?php get_search_form(); ?>
              </div>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-bars"></i>
            </button>
          </div>
        </nav>
    </header>
