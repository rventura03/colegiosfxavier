<?php get_header();?>

<section class="main">
	<div class="container quemSomos">
		<h1><?=the_title()?></h1>
		<div class="aboutusSubtitle">
			<?= the_field("aboutus_citacao")?>
		</div>
		<div class="aboutusText">
			<?= the_field("textContent")?>
		</div>
		<div class="row chronology">
			<div class="scroller">
				<?php if( have_rows('cronologia') ):?>
				    <?php while( have_rows('cronologia') ): the_row();?>
							<div data-chronology="<?=get_row_index(); ?>" class="col-md-2 col-sm-6 col-5 chronologyItem">
								<div class="iconChronology"><i class="fas fa-sort-down"></i></div>
								<div class="yearChronology"><?php the_sub_field('ano'); ?></div>
								<div class="legendChronology"><p><?php the_sub_field('titulo'); ?></p></div>
							</div>
						<?php endwhile; ?>
				<?php endif; ?>
			</div>

			<?php if( have_rows('cronologia') ):?>
					<?php while( have_rows('cronologia') ): the_row();?>
						<div data-content="<?=get_row_index(); ?>" class="chronologyContent" style="display: none;">
							<div onclick="closeContent()" class="closeContent"><i class="far fa-times-circle"></i></div>
							<?php the_sub_field('textoContent'); ?>
						</div>
					<?php endwhile; ?>
			<?php endif; ?>
		</div>
		<div class="values">
			<h1>Os Nossos Valores</h1>
			<div class="row m-0">
				<div class="col-12 col-sm-12 col-md-4 block1">
					<i class="fas fa-user-check"></i>
					<p>Confiamos nos nossos Colaboradores</p>
				</div>
				<div class="col-12 col-sm-12 col-md-4 block2">
					<i class="fas fa-heart"></i>
					<p>Trabalhamos com Paixão</p>
				</div>
				<div class="col-12 col-sm-12 col-md-4 block3">
					<i class="fas fa-check-circle"></i>
					<p>Educamos para a Excelência</p>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer();?>


<script>
	$('.chronologyItem').click(function(e){
 	e.preventDefault();
		var chronItem = $(this).data('chronology');
			$('.chronologyContent').each(function( index ) {
					$(this).hide();
					$('.chronologyItem').removeClass('selected');
			});
			$('.chronologyItem[data-chronology=' + chronItem + ']').addClass('selected');
			$('.chronologyContent[data-content=' + chronItem + ']').fadeIn();
	});
	function closeContent(){
		$('.chronologyContent').fadeOut('slide');
		$('.chronologyItem.selected').removeClass('selected');
	}
</script>
