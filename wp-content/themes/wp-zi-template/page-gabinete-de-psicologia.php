<?php get_header();?>

<section class="main">
	<div class="container gabinetePsicologia">
		<div class="titlegabinetePsicologia">
			<h2 class="titleservicosOpcionais"><?=the_title()?></h2>
		</div>
		<div class="contentgabinetePsicologia">
		<?php if( have_rows('foto_psicologa') ): ?>
			<?php while( have_rows('foto_psicologa') ): the_row(); 

				// Get sub field values.
				$image = get_sub_field('fotoPsicologa');
				$legend = get_sub_field('fotoLegenda');

			?>
				<div class="fotoPsicologa">
					<img src="<?=$image?>" alt="<?=$legend?>"/>
					<p><?=$legend?></p>
				</div>
			<?php
				endwhile;
			endif;
			?>
			<div class="contentText">
				<?php the_field("textContent"); ?>
			</div>
			<?php 
				if (have_rows('gp_docCat')):
			?>
			<div class="filesPsicologia">
				<div class="row m-0">
					<div class="col-12 col-sm-12 col-md-12 col-lg-4 filesPsicologiaTitle">
						<p class="filesTitle">Descarregue os ficheiros PDF aqui</p>
					</div>
					<div class="col-12 col-sm-12 col-md-12 col-lg-8 filesPsicologiaButtons">
						<div class="fileSlider">
							<?php if( have_rows('gp_docCat') ):
								?>
								<?php
								$i = "1";
								while( have_rows('gp_docCat') ): the_row();

								$title = get_sub_field('catTitle');
								$icon = get_sub_field('catImg');
								$document = get_sub_field('catFiles');
									echo '
									<div class="slick-slide row buttonFiles m-0">
										<div class="col itemFile" style="background-color:';
										if($i == "1"){
											echo '#1b2376;">';
										}else if($i == "2"){
											echo '#4169d6;">';
										}else if($i == "3"){
											echo '#51b5ee;">';
										}
										echo'<span class="topIcon"><i class="fas fa-download"></i></span>
											<span class="fileIcon"><img src="'.$icon.'" alt="Icon do Documento"/></span>
											<div class="filesList" style="display: none;">
												<div class="fListscroll">';
													while( have_rows('catFiles') ): the_row();
														$fileTitle = get_sub_field('titleFile');
														$fileUrl = get_sub_field('docFile');
														echo '<a href="'. $fileUrl .'" target="_blank"><p><span><i class="far fa-file-alt"></i></span>'. $fileTitle .'</p></a>';
													endwhile;	
											echo'</div></div>
											<p class="fileTitle">'.$title.'</p>
											<div class="arrowDown'.$i.'"></div>
										</div>
									</div>';
									if ($i != "3"){
										$i++;
									}else{
										$i = "1";
									}
								?>
								<?php endwhile;?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
			<?php 
				endif;
			?>
			<!--<div id="contentSlide" class="container">
				<div id="item1" class="itemSection linkTab1" data-slide="1" style="display: block;">
					<div class="row sliderofertaFormativa">
						<div class="col-12 col-sm-12 col-md-12 col-lg-4 itemTitle" syle="border: 1px solid;">
							<p>Salas de Estudo</p>
						</div>
						<div class="col-12 col-sm-12 col-md-12 col-lg-8 itemImages" syle="border: 1px solid;">
								<?php if( have_rows('SEimgs') ): ?>
									<div class="itemSlider" id="itemSlider">
								<?php while( have_rows('SEimgs') ): the_row(); ?>
										<div class="item">
											<img src="<?php the_sub_field('SE_imgs'); ?>" alt="trans" width="310" height="310">
											<div class="show-icon">
												<i class="fas fa-search-plus"></i>
											</div>
										</div>
								<?php endwhile; ?>
								<?php endif; ?>
							</div>
						</div>
					</div>
					<div class="textofertaFormativa">
						<?=the_field('SE_texto')?>
					</div>
					<?php 
						if (have_rows('salaestudo_docCat')):
					?>
					<div class="filesofertaFormativa">
						<div class="row m-0">
							<div class="col-12 col-sm-12 col-md-12 col-lg-4 filesofertaformativaTitle">
								<p class="filesTitle">Descarregue os ficheiros PDF aqui</p>
							</div>
							<div class="col-12 col-sm-12 col-md-12 col-lg-8 filesofertaformativaButtons">
								<div class="fileSlider">
									<?php if( have_rows('salaestudo_docCat') ):
										?>
										<?php
										$i = "1";
										while( have_rows('salaestudo_docCat') ): the_row();
	
										$title = get_sub_field('catTitle');
										$icon = get_sub_field('catImg');
										$document = get_sub_field('catFiles');
											echo '
											<div class="slick-slide row buttonFiles m-0">
												<div class="col itemFile" style="background-color:';
												if($i == "1"){
													echo '#1b2376;">';
												}else if($i == "2"){
													echo '#4169d6;">';
												}else if($i == "3"){
													echo '#51b5ee;">';
												}
												echo'<span class="topIcon"><i class="fas fa-download"></i></span>
													<span class="fileIcon"><img src="'.$icon.'" alt="Icon do Documento"/></span>
													<div class="filesList" style="display: none;">
														<div class="fListscroll">';
															while( have_rows('catFiles') ): the_row();
																$fileTitle = get_sub_field('titleFile');
																$fileUrl = get_sub_field('docFile');
																echo '<a href="'. $fileUrl .'" target="_blank"><p><span><i class="far fa-file-alt"></i></span>'. $fileTitle .'</p></a>';
															endwhile;	
													echo'</div></div>
													<p class="fileTitle">'.$title.'</p>
													<div class="arrowDown'.$i.'"></div>
												</div>
											</div>';
											if ($i != "3"){
												$i++;
											}else{
												$i = "1";
											}
										?>
										<?php endwhile;?>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
					<?php 
						endif;
					?>
				</div>
	
				<div id="item2" class="itemSection linkTab2" data-slide="2" style="display: none;">
					<div class="row sliderofertaFormativa">
						<div class="col-12 col-sm-12 col-md-12 col-lg-4 itemTitle" syle="border: 1px solid;">
							<p>Prolongamento</p>
						</div>
						<div class="col-12 col-sm-12 col-md-12 col-lg-8 itemImages" syle="border: 1px solid;">
								<?php if( have_rows('prolongamentoEscolarimgs') ): ?>
									<div class="itemSlider" id="itemSlider">
								<?php while( have_rows('prolongamentoEscolarimgs') ): the_row(); ?>
										<div class="item">
											<img src="<?php the_sub_field('prolongamento_imgs'); ?>" alt="trans" width="310" height="310">
											<div class="show-icon">
												<i class="fas fa-search-plus"></i>
											</div>
										</div>
								<?php endwhile; ?>
								<?php endif; ?>
							</div>
						</div>
					</div>
					<div class="textofertaFormativa">
						<?= the_field('prolongamento_texto')?>
					</div>
					<?php 
						if (have_rows('prolongamento_docCat')):
					?>
					<div class="filesofertaFormativa">
						<div class="row m-0">
							<div class="col-12 col-sm-12 col-md-12 col-lg-4 filesofertaformativaTitle">
								<p class="filesTitle">Descarregue os ficheiros PDF aqui</p>
							</div>
							<div class="col-12 col-sm-12 col-md-12 col-lg-8 filesofertaformativaButtons">
								<div class="fileSlider">
									<?php if( have_rows('prolongamento_docCat') ):
										?>
										<?php
										$i = "1";
										while( have_rows('prolongamento_docCat') ): the_row();
	
										$title = get_sub_field('catTitle');
										$icon = get_sub_field('catImg');
										$document = get_sub_field('catFiles');
											echo '
											<div class="slick-slide row buttonFiles m-0">
												<div class="col itemFile" style="background-color:';
												if($i == "1"){
													echo '#1b2376;">';
												}else if($i == "2"){
													echo '#4169d6;">';
												}else if($i == "3"){
													echo '#51b5ee;">';
												}
												echo'<span class="topIcon"><i class="fas fa-download"></i></span>
													<span class="fileIcon"><img src="'.$icon.'" alt="Icon do Documento"/></span>
													<div class="filesList" style="display: none;">
														<div class="fListscroll">';
															while( have_rows('catFiles') ): the_row();
																$fileTitle = get_sub_field('titleFile');
																$fileUrl = get_sub_field('docFile');
																echo '<a href="'. $fileUrl .'" target="_blank"><p><span><i class="far fa-file-alt"></i></span>'. $fileTitle .'</p></a>';
															endwhile;	
													echo'</div></div>
													<p class="fileTitle">'.$title.'</p>
													<div class="arrowDown'.$i.'"></div>
												</div>
											</div>';
											if ($i != "3"){
												$i++;
											}else{
												$i = "1";
											}
										?>
										<?php endwhile;?>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
					<?php 
						endif;
					?>
				</div>
	
				<div id="item3" class="itemSection linkTab3" data-slide="3" style="display: none;">
					<div class="row sliderofertaFormativa">
						<div class="col-12 col-sm-12 col-md-12 col-lg-4 itemTitle" syle="border: 1px solid;">
							<p>Campo de Férias</p>
						</div>
						<div class="col-12 col-sm-12 col-md-12 col-lg-8 itemImages" syle="border: 1px solid;">
								<?php if( have_rows('CFimgs') ): ?>
									<div class="itemSlider" id="itemSlider">
								<?php while( have_rows('CFimgs') ): the_row(); ?>
										<div class="item">
											<img src="<?php the_sub_field('CF_imgs'); ?>" alt="trans" width="310" height="310">
											<div class="show-icon">
												<i class="fas fa-search-plus"></i>
											</div>
										</div>
								<?php endwhile; ?>
								<?php endif; ?>
							</div>
						</div>
					</div>
					<div class="textofertaFormativa">
						<?= the_field('CF_texto')?>
					</div>
					<?php 
						if (have_rows('campoferias_docCat')):
					?>
					<div class="filesofertaFormativa">
						<div class="row m-0">
							<div class="col-12 col-sm-12 col-md-12 col-lg-4 filesofertaformativaTitle">
								<p class="filesTitle">Descarregue os ficheiros PDF aqui</p>
							</div>
							<div class="col-12 col-sm-12 col-md-12 col-lg-8 filesofertaformativaButtons">
								<div class="fileSlider">
									<?php if( have_rows('campoferias_docCat') ):
										?>
										<?php
										$i = "1";
										while( have_rows('campoferias_docCat') ): the_row();
	
										$title = get_sub_field('catTitle');
										$icon = get_sub_field('catImg');
										$document = get_sub_field('catFiles');
											echo '
											<div class="slick-slide row buttonFiles m-0">
												<div class="col itemFile" style="background-color:';
												if($i == "1"){
													echo '#1b2376;">';
												}else if($i == "2"){
													echo '#4169d6;">';
												}else if($i == "3"){
													echo '#51b5ee;">';
												}
												echo'<span class="topIcon"><i class="fas fa-download"></i></span>
													<span class="fileIcon"><img src="'.$icon.'" alt="Icon do Documento"/></span>
													<div class="filesList" style="display: none;">
														<div class="fListscroll">';
															while( have_rows('catFiles') ): the_row();
																$fileTitle = get_sub_field('titleFile');
																$fileUrl = get_sub_field('docFile');
																echo '<a href="'. $fileUrl .'" target="_blank"><p><span><i class="far fa-file-alt"></i></span>'. $fileTitle .'</p></a>';
															endwhile;	
													echo'</div></div>
													<p class="fileTitle">'.$title.'</p>
													<div class="arrowDown'.$i.'"></div>
												</div>
											</div>';
											if ($i != "3"){
												$i++;
											}else{
												$i = "1";
											}
										?>
										<?php endwhile;?>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
					<?php 
						endif;
					?>
				</div>
			</div>-->
		</div>
	</div>
</section>




<script>
//List Files by category
$(".buttonFiles").click (function(e){
	e.stopPropagation();
	$(".buttonFiles .filesList").fadeOut();
	$(".buttonFiles .fileIcon").fadeIn();
	var idthis = $(this).attr("data-slick-index");
	console.log('.buttonFiles[data-slick-index="' + idthis + '"]');
	if ($('.buttonFiles[data-slick-index="' + idthis + '"] .filesList').css("display") == "block"){
		$(".buttonFiles .filesList").fadeOut();
		$(".buttonFiles .fileIcon").fadeIn();
	}else{
		$('.buttonFiles[data-slick-index="' + idthis + '"] .filesList').fadeIn();
		$('.buttonFiles[data-slick-index="' + idthis + '"] .fileIcon').fadeOut();
	}
});
$(window).click (function(){
	$(".fileSlider .buttonFiles .filesList").fadeOut();
	$(".fileSlider .buttonFiles .fileIcon").fadeIn();
});


$('.fileSlider').slick({
	autoplay: false,
	dots: true,
	arrows: true,
	slidesToShow: 3,
	slidesToScroll: 1,
	infinite: false,
	responsive: [
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
		slidesToScroll: 1,
		dots: false
      }
	},
	{
      breakpoint: 365,
      settings: {
        slidesToShow: 1,
		slidesToScroll: 1,
		dots: false
      }
    }]

});



</script>



<?php get_footer();?>
