<?php get_header();?>

<section class="main">
	<div class="container organogramaColegio">
		<h1 class="titleOrganograma">Organograma do Colégio</h1>
		<div class="contentOrganograma">
			<a href="<?=the_field("imagem_organograma")?>" target="_blank" title="Organograma do Colégio"><img src="<?=the_field("imagem_organograma")?>" alt="Organograma do Colégio"/></a>
		</div>
	</div>
</section>

<?php get_footer();?>


<script>
jQuery(document).ready(function(){
	jQuery('.scroller').scrollbar();
});
</script>
