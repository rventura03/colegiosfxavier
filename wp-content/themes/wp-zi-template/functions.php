<?php
add_theme_support( 'menus' );
add_theme_support('post-thumbnails');


// REMOVE WP EMOJI
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


// Evita caracteres especiais no upload de ficheiros
function wpartisan_sanitize_file_name( $filename ) {

	$sanitized_filename = remove_accents( $filename ); // Convert to ASCII

	// Standard replacements
	$invalid = array(
		' '   => '-',
		'%20' => '-',
		'_'   => '-',
	);
	$sanitized_filename = str_replace( array_keys( $invalid ), array_values( $invalid ), $sanitized_filename );

	$sanitized_filename = preg_replace('/[^A-Za-z0-9-\. ]/', '', $sanitized_filename); // Remove all non-alphanumeric except .
	$sanitized_filename = preg_replace('/\.(?=.*\.)/', '', $sanitized_filename); // Remove all but last .
	$sanitized_filename = preg_replace('/-+/', '-', $sanitized_filename); // Replace any more than one - in a row
	$sanitized_filename = str_replace('-.', '.', $sanitized_filename); // Remove last - if at the end
	$sanitized_filename = strtolower( $sanitized_filename ); // Lowercase

	return $sanitized_filename;
}
add_filter( 'sanitize_file_name', 'wpartisan_sanitize_file_name', 10, 1 );




function wpse_hide_cat_descr() { ?>

    <style type="text/css">
       .term-description-wrap {display: none;}
       .term-slug-wrap {display: none;}
    </style>
<?php
 }

add_action( 'admin_head-term.php', 'wpse_hide_cat_descr' );
add_action( 'admin_head-edit-tags.php', 'wpse_hide_cat_descr' );
//add_action( 'admin_notices', 'dependencias_seguranca' );

function dependencias_seguranca() {
	if( !is_plugin_active( 'wordfence/wordfence.php' ) ){
	   $url = admin_url('plugin-install.php?s=wordfence&tab=search&type=term');
       echo '<div class="error"><p>' . __( '<b>AVISO:</b> Instalar <a href="'.$url.'">WordFence</a>', 'Acorespro' ) . '</p></div>';
    }
	if( !is_plugin_active( 'loginizer/loginizer.php' ) ){
        $url = admin_url('plugin-install.php?s=loginizer&tab=search&type=term');
		echo '<div class="error"><p>' . __( '<b>AVISO:</b> Instalar <a href="'.$url.'">Loginizer</a>', 'Acorespro' ) . '</p></div>';
    }
}

//---------Tamanho Imagens personalizadas--------------
/*add_image_size( 'icons', 64 );
add_image_size( 'produto_destaque', 350 );*/

// Custom Excerpt
function custom_trim_words( $text, $num_words = 55, $more = null ) {
    if ( null === $more )
        $more = __( '&hellip;' );
    $original_text = $text;
    $text = strip_shortcodes( $text );
    // Add tags that you don't want stripped
    $text = strip_tags( $text, '<strong>, <b>, <em>, <i>, <p>, <br />. <br>' );
    if ( 'characters' == _x( 'words', 'word count: words or characters?' ) && preg_match( '/^utf\-?8$/i', get_option( 'blog_charset' ) ) ) {
        $text = trim( preg_replace( "/[\n\r\t ]+/", ' ', $text ), ' ' );
        preg_match_all( '/./u', $text, $words_array );
        $words_array = array_slice( $words_array[0], 0, $num_words + 1 );
        $sep = '';
    } else {
        $words_array = preg_split( "/[\n\r\t ]+/", $text, $num_words + 1, PREG_SPLIT_NO_EMPTY );
        $sep = ' ';
    }
    if ( count( $words_array ) > $num_words ) {
        array_pop( $words_array );
        $text = implode( $sep, $words_array );
        $text = $text . $more;
    } else {
        $text = implode( $sep, $words_array );
    }
    return apply_filters( 'custom_trim_words', $text, $num_words, $more, $original_text );
}

function wpb_change_title_text( $title ){
     $screen = get_current_screen();

     if  ( 'testemunhos' == $screen->post_type ) {
          $title = 'Nome...';
     }

     return $title;
}

add_filter( 'enter_title_here', 'wpb_change_title_text' );

function customize_post_admin_menu_labels() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Notícias';
    $submenu['edit.php'][5][0] = 'Notícias';
    $submenu['edit.php'][10][0] = 'Adicionar Notícias';
}
add_action( 'admin_menu', 'customize_post_admin_menu_labels' );

function example_admin_bar_remove_logo() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu( 'wp-logo' );
}
add_action( 'wp_before_admin_bar_render', 'example_admin_bar_remove_logo', 0 );

function redirect_cf7() {
    ?>
<script type="text/javascript">
document.addEventListener( 'wpcf7mailsent', function( event ) {
    if ( '391' == event.detail.contactFormId ) { // Sends sumissions on form 947 to the first thank you page
        location = 'http://zonadeideias.pt/colegiosfx/newsletter-success/';
    } 
}, false );
</script>
<?php
}

add_action( 'wp_footer', 'redirect_cf7' );

/**
 * Filters the HTML format of the search form.
 *
 * @since 3.6.0
 *
 * @param string $format The type of markup to use in the search form.
 *                       Accepts 'html5', 'xhtml'.
 */
$format = apply_filters( 'search_form_format', $format );

$search_form_template = locate_template( 'searchform.php' );
if ( '' != $search_form_template ) {
	ob_start();
	require $search_form_template;
	$form = ob_get_clean();
} else {
	// Build a string containing an aria-label to use for the search form.
	if ( isset( $args['aria_label'] ) && $args['aria_label'] ) {
		$aria_label = 'aria-label="' . esc_attr( $args['aria_label'] ) . '" ';
	} else {
		/*
			* If there's no custom aria-label, we can set a default here. At the
			* moment it's empty as there's uncertainty about what the default should be.
			*/
		$aria_label = '';
	}
	if ( 'html5' == $format ) {
		$form = '<form role="search" ' . $aria_label . 'method="get" class="search-form" action="' . esc_url( home_url( '/' ) ) . '">
			<label>
				<span class="screen-reader-text">' . _x( 'Search for:', 'label' ) . '</span>
				<input type="search" class="search-field" placeholder="' . esc_attr_x( 'Search &hellip;', 'placeholder' ) . '" value="' . get_search_query() . '" name="s" />
			</label>
			<input type="submit" class="search-submit" value="' . esc_attr_x( 'Search', 'submit button' ) . '" />
		</form>';
	} else {
		$form = '<form role="search" ' . $aria_label . 'method="get" id="searchform" class="searchform" action="' . esc_url( home_url( '/' ) ) . '">
			<div>
				<label class="screen-reader-text" for="s">' . _x( 'Search for:', 'label' ) . '</label>
				<input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="Pesquisar"/>
				<button type="submit" id="searchsubmit"><i class="fas fa-search"></i></button>
			</div>
		</form>';
	}
}

remove_action( 'wp_head', 'wp_print_styles', 8 );
remove_action( 'wp_head', 'wp_print_head_scripts', 9 );