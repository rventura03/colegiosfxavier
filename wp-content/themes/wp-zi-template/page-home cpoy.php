<?php get_header();?>

<section class="home-carousel">
	<div class="container-fluid p-0">
		<div class="slick-track">
			<?php if( have_rows('home_slider') ):?>

			    <?php while( have_rows('home_slider') ): the_row();?>
						<div class="slick-slide">
							<img src="<?php the_sub_field('homeSlider-img'); ?>" alt="Home Slider Image"/>
						</div>
			    <?php endwhile; ?>
			<?php endif; ?>
		</div>
		<div class="container">
			<p><span>126 anos</span> ao serviço da <span>educação dos Açores</span></p>
			<div class="info-button">
				<button type="button" class="btn" onclick="window.location.href='oferta-formativa'">Oferta Formativa</button>
			</div>
		</div>
	</div>
</section>
<section class="main">
	<div class="container-fluid highlightedNews p-0">
		
		<?php
	    $news_args = array('category_name' => 'destaque',
										'post_type' =>  'post',
										'posts_per_page' => -1  );
			$news_query = new WP_Query($news_args);
			if ($news_query->have_posts()):?>
				<div class="news-track">
				<?php while ($news_query->have_posts()):$news_query->the_post();?>

				<div class="news-slide">
					<div class="row m-0">
						<div class="col-12 col-lg-6 p-0 news-img" style="background-image: url('<?php the_post_thumbnail_url(); ?>')">
							<div class="content">
								<div class="news-spotlight"><span><i class="fas fa-star"></i></span><p>Notícias em destaque</p></div>
								<h2><?php echo $post->post_title; ?></h2>
								<h3><?php the_field('subtitulo');?></h3>
								<div class="post-link d-lg-none d-xl-none"><a href="<?php the_permalink(); ?>">Ler mais<i class="fas fa-arrow-right"></i></a></div>
							</div>
						</div>
						<div class="col-6 p-0 d-none d-sm-none d-md-none d-lg-block news-content">
							<div class="content">
								<div class="description"><?php echo wp_trim_words( get_the_content(), 46); ?></div>
								<div class="post-link"><a href="<?php the_permalink(); ?>">Ler mais<i class="fas fa-arrow-right"></i></a></div>
							</div>
						</div>
					</div>
				</div>
			<?php
			endwhile;?>
			</div>
		<?php endif;
		wp_reset_query();
		?>
	</div>
	<div class="container-fluid transparency px-0">
		<div class="row m-0">
			<div class="col-sm-12 col-md-6 title pl-0">
				<div class="content">
					<p>São Francisco Xavier, <span><br>um colégio de confiança</span></p>
				</div>
			</div>
			<div class="col-sm-12 col-md-6 slider p-0">
				<div class="transparencyslider">
					<?php
					if( have_rows('transparency') ):
				    while ( have_rows('transparency') ) : the_row();?>
							<div class="item">
								<img src="<?php the_sub_field('transparency-img')?>" alt="trans" width="310" height="310">
								<div class="show-icon">
									<i class="fas fa-search-plus"></i>
								</div>
							</div>
				   <?php endwhile; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid testimonials p-0">
		<h3>Testemunhos</h3>
		<div class="testimonial-track">
			<?php $args = array( 'category' => 0, 'post_type' =>  'testemunhos', 'posts_per_page' => -1 );
			$postslist = get_posts( $args );
			$len = count($postslist);
			$len = $len - 1;
			$i = 1;
			foreach ($postslist as $post) :  setup_postdata($post);
				if ($i % 2):?>
						<div class="row m-0">
							<div class="col-6 col-sm-12 testimonial-main"><!--1ST PERSON-->
								<div class="col-6 testimonial-personInfo" style="background-image: url('<?php the_post_thumbnail_url(); ?>')">
									<p class="person-name"><?= $post->post_title; ?></p>
									<p class="person-work"><?= get_field('emprego');?></p>
								</div>
								<div class="col-6 testimonial-text">
									<p><?= get_the_content(); ?></p>
								</div>
							</div>
							<?php else:?>
							<div class="col-6 col-sm-12 testimonial-main"><!--2ND PERSON-->
								<div class="col-6 testimonial-text">
									<p><?= get_the_content(); ?></p>
								</div>
								<div class="col-6 testimonial-personInfo" style="background-image: url('<?php the_post_thumbnail_url(); ?>')">
									<p class="person-name"><?= $post->post_title; ?></p>
									<p class="person-work"><?= get_field('emprego');?></p>
								</div>
							</div>
						</div>
			<?php
				endif;
				$i++;
			endforeach;?>
		</div>
	</div>
	<div class="container-fuild highlights">
		<div class="container">
			<div class="row">
				<div class="slideHighlights">
					<?php
			    $args = array( 'category' => 0, 'post_type' =>  'destaques', 'posts_per_page' => 4  );
			    $postslist = get_posts( $args );
			    foreach ($postslist as $post) :  setup_postdata($post);
					//print_r($post);
			    ?>
					<div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 my-4 highlightItem">
						<div class="newsMain">
							<div class="newsImage">
								<img src="<?php the_post_thumbnail_url(); ?>" alt="newsImage"/>
							</div>
							<div class="newsContent">
								<div class="newsHeader">
									<h2><?= $post->post_title ?></h2>
									<h3><?= get_field('subtitulo')?></h3>
								</div>
								<div class="newsExcerpt">
									<?= wp_trim_words( get_the_content(), 20); ?>
								</div>
								<div class="newsReadMore">
									<a href="<?php the_permalink(); ?>">Ler mais<i class="fas fa-arrow-right"></i></a>
								</div>
								<div class="diagonalSeparator"></div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
				</div>
			</div>
			<img class="bottomLogo" src="<?=content_url();?>/uploads/2020/02/csfx-logogold.png" alt="csfxLogo"/>
		</div>
	</div>
	<div class="container-fuild events">
		<h3>Acontece no São Francisco Xavier</h3>
		<div class="container">
				<?php
				$args = array( 'category' => 0,
				'post_type' =>  'eventos',
				'posts_per_page' => -1,
				'meta_key' => 'data',
      	'orderby' => 'meta_value',
        'order' => 'ASC',  );
				$postslist = get_posts( $args );
				$i=0;
				$slide=0;

				if (empty($postslist)){
					echo '<p style="text-align: center; font-size: 16px; color: #1c4a80; font-weight: 500;">Neste momento estamos sem eventos.';
				}
				?>
				<div class="sliderEvents">
				<?php
				foreach ($postslist as $post) :  setup_postdata($post);
				$data = get_field('data');
				//echo $data;
				$Tdate = date('d/m/Y');
				?>
				<div class="row px-4 <?php if ($data == $Tdate):echo'selectedEvent'; endif;?>"  <?php if ($data == $Tdate): $slide=$i; endif;?>> <!--slick-current-->
					<?php $i++;?>
					<div class="col-sm-12 p-0">
						<div class="eventLeft">
							<?php
								$fieldDate = get_field('data');
								$day = substr($fieldDate, 0, 2);
								$month = substr($fieldDate, 3, 2);
								switch ($month) {
							    case "01":
							        $month = "Jan";
							        break;
									case "02":
											$month = "Fev";
											break;
									case "03":
											$month = "Mar";
											break;
									case "04":
											$month = "Abr";
											break;
									case "05":
											$month = "Mai";
											break;
									case "06":
											$month = "Jun";
											break;
									case "07":
											$month = "Jul";
											break;
									case "08":
											$month = "Ago";
											break;
									case "09":
											$month = "Set";
											break;
									case "10":
											$month = "Out";
											break;
									case "11":
											$month = "Nov";
											break;
									case "12":
											$month = "Dez";
											break;
									}
							?>
							<div class="eventMonth">
								<p class="monthIcon"><i class="fas fa-calendar-check"></i></p>
								<p class="month"><?=$month;?></p>
							</div>
							<div class="eventDay">
								<p class="day"><?=$day;?></p>
							</div>
							<div class="bottomIcon">
								<i class="fas fa-calendar-check"></i>
							</div>
						</div>
						<div class="eventRight">
							<p class="eventTitle"><?= $post->post_title;?></p>
							<p class="eventsubTitle"><?= get_field('subtitulo');?></p>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">


$('.slick-track').slick({
	autoplay: true,
	dots: true,
	speed: 800,
	responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false
      }
    },
		{
			breakpoint: 992,
			settings: {
				arrows: false
			}
		}]
});

$('.slideHighlights').slick({
	slidesToShow: 4,
	slidesToScroll: 4,
	autoplay: true,
	responsive: [
		{
			breakpoint: 576,
			settings: {
					slidesToShow: 1,
				slidesToScroll: 1,
				dots: true,
				arrows: false
			}
		},
		{
			breakpoint: 768,
			settings: {
					slidesToShow: 2,
				slidesToScroll: 2,
				dots: true,
				arrows: false
			}
		},
		{
			breakpoint: 992,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2,
				dots: true,
				arrows: false
			}
		}]
});

$('.news-track').slick({
	autoplay: true,
	dots: true,
	arrows: false
});

$('.transparencyslider').slick({
	autoplay: true,
	dots: true,
	arrows: true,
	slidesToShow: 3,
	slidesToScroll: 3,
	responsive: [
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    },
		{
      breakpoint: 922,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    },
		{
      breakpoint: 1200,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
      }
    }
	],
});

$('.transparencyslider').slickLightbox({
  src: 'src',
  itemSelector: '.item img'
});

$('.testimonial-track').slick({
	autoplay: false,
	dots: false,
	arrows: true,
	slidesToShow: 2,
	infinite: true,
	slidesToScroll: 1,
	centerMode: true,
	respondTo: 'window',
	responsive: [
    {
      breakpoint: 1441,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,

				centerMode: true
      }
    },
		{
      breakpoint: 1317,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,

				centerMode: true
      }
    },
		{
      breakpoint: 991,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 4,

				centerMode: true
      }
    },
		{
      breakpoint: 765,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 3,
				centerMode: true
      }
    },
		{
      breakpoint: 575,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
				centerMode: true
      }
    },
  ],
});

$('.sliderEvents').slick({
	autoplay: true,
	dots: false,
	arrows: true,
	slidesToShow: 3,
	slidesToScroll: 4,
	infinite: true,
	centerMode: true,
	responsive: [
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    },
		{
      breakpoint: 922,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
      }
    },
		{
      breakpoint: 1200,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
      }
    }
	],
});

//tesimonial expand text

$(".testimonial-main").click(function (e){
	var test = $(this);
	//alert($(".testimonial-main").hasClass("active"));
	if (test.hasClass("active")){
		$(test).removeClass("active");
	}else{
		$(test).removeClass("active");
		$(test).addClass("active");
	}
});


//var slid=parseInt($('.selectedEvent').data('slick-index')*(-1));
var slid=<?=$slide;?>;
var currentSlide = $('.sliderEvents').slick('slickGoTo',slid);



</script>

<?php get_footer();?>
