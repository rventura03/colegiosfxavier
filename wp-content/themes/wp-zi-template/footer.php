<footer>
  <div class="container-fluid footer-title">
    <h3>Fique em contato</h3>
  </div>
  <div class="container-fluid footer-content p-0">
    <div class="container ">
      <div class="row keep-touch">
        <div id="contactIcon" class="col-6 col-sm-6 col-lg-2 icon-contactform" onclick="openContactform()">
          <div class="under-contactformicon"><i class="far fa-comment"></i></div>
          <p><i class="far fa-comment"></i></p>
        </div>
        <div id="contactContent" class="col-4 content-contactform">
          <p class="content-title">Fale connosco.</p>
          <p class="content-text" onclick="openContactform()">Clique para abrir a janela</p>
        </div>
        <div id="newsletterIcon" class="col-6 col-sm-6 col-lg-2 icon-newsletter" onclick="openNewsletterform()">
          <div class="under-newslettericon"><i class="far fa-envelope-open"></i></div>
          <p><i class="far fa-envelope-open"></i></p>
        </div>
        <div id="newsletterContent" class="col-4 content-newsletter">
          <p class="content-title">Adira à nossa newsletter.</p>
          <p class="content-text" onclick="openNewsletterform()">Clique para abrir a janela</p>
        </div>
        <div id="contactForm" class="col-12 col-lg-10 content-contactform expanded" style="display: none;">
          <div class="closeExpanded" onclick="closeContacts()"><i class="fas fa-times"></i></div>
          <div class="row expandedContactform m-0">
            <?= do_shortcode('[contact-form-7 id="390" title="Formulário de contacto"]');?>
          </div>
        </div>
        <div id="newsletterForm" class="col-12 col-lg-10 content-newsletter expanded" style="display:none;">
          <div class="closeExpanded"  onclick="closeContacts()"><i class="fas fa-times"></i></div>
          <div class="row expandedNewsletterform m-0">
            <div class="WrapperexpandedNewsletterform">
              <?= do_shortcode('[contact-form-7 id="391" title="Newsletter"]');?>
            </div>
          </div>
        </div>
      </div>
      <div class="row informations m-0">
        <div class="col-12 col-lg-6 col-xl-4 contacts-section">
          <div class="title"><p>Contactos</p></div>
          <div class="content">
            <ul>
              <li><i class="fas fa-phone-alt"></i> <a href="tel:296205200" title="Telefone">296 205 200</a></li>
              <li><i class="fas fa-envelope"></i> <a href="mailto:info@colegiosfxavier.com" title="Email">info@colegiosfxavier.com</a></li>
              <li><i class="fas fa-home"></i> <a href="https://www.google.pt/maps/dir//Col%C3%A9gio+de+S%C3%A3o+Francisco+Xavier,+R.+de+S%C3%A3o+Joaquim+801,+Ponta+Delgada/@37.7444121,-25.675054,18.5z/data=!4m8!4m7!1m0!1m5!1m1!1s0xb432acf586b443d:0x25e1de3e07cecd55!2m2!1d-25.6745962!2d37.7449264?hl=pt-PT" target="_blank" title="Endereço">Rua Agostinho Pacheco Apartado 1393<br>9501-801 Ponta Delgada</a></li>
            </ul>
            <div class="row policies-links d-none d-lg-flex">
              <div class="col-6">
                <a href="politica-privacidade" title="Política de Privacidade">Politica de Privacidade e Segurança</a>
              </div>
              <div class="col-6">
                <a href="politica-cookies" title="Política de Cookies">Politica de Cookies</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-lg-6 col-xl-3 info-section">
          <div class="title"><p>Horário Semanal</p></div>
          <div class="content">
            <p class="availability-time"><i class="far fa-clock"></i> Segunda a Sexta 07h30 - 19h00</p>
            <p class="follow-facebook">Siga-nos na rede social Facebook <span><a href="https://www.facebook.com/Col%C3%A9gio-De-S%C3%A3o-Francisco-Xavier-383576428467465/" target="_blank" title="Facebook"><i class="fab fa-facebook"></i></a></span></p>
          </div>
        </div>
        <div class="col-12 col-lg-12 col-xl-5 menu-section">
          <h3 class="menuTitle d-block d-lg-none" onclick="toggleBottomMenu()">Menu <i class="fas fa-chevron-down"></i></h3>
          <div class="bottomMenu d-lg-block">
            <?php
              wp_nav_menu();
            ?>
          </div>
          <div class="row policies-links d-flex d-lg-none">
            <div class="col-6">
              <a href="politica-privacidade" title="Política de Privacidade">Politica de Privacidade e Segurança</a>
            </div>
            <div class="col-6">
              <a href="politica-cookies" title="Política de Cookies">Politica de Cookies</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <hr class="footer-line">
    <div class="container bottom-rights">
      <p class="csfx-rights">&#xA9; 2020 csfx | Todos os direitos reservados<br>Desenvolvido por <a href="http://zonadeideias.pt">zona<span>de</span>ideias</a></p>
      <a href="http://www.clunyportugal.com/" target="_blank"><img class="icon-cluny" src="<?=content_url();?>/uploads/2020/01/logotipo-sjcluny.png" alt="São José de Cluny"></a>
    </div>
  </div>
</footer>
<?php
  $marquee_args = array(
   'post_type' => 'destaque_rotativo',
   'order' => 'DESC',
   'posts_per_page' => -1);
  $marquee_query = new WP_Query($marquee_args);
  if ($marquee_query->have_posts()):?>
  <div class="marqueeContainer">
    <marquee behavior="scroll" scrolldelay="100" scrollamount="14" direction="left" loop="true" width="100%" onMouseOver="stop()" onMouseOut="start()">
    <?php while ($marquee_query->have_posts()):$marquee_query->the_post();?>
      <div class="marqueeItem"><span class="marqueeTitle"><?php the_title();?></span><span class="marqueeText"><?php the_field('legenda')?></span></div>
    <?php endwhile; ?>
    </marquee>
  </div>
  <?php endif; ?>





<script>
$("#mobileExpand" ).click(function() {
  $("#topnavContent").slideToggle("swing", function() {

  });
});
//menu/sub-menu 
$( window ).resize(function() {
  $('.menu-item').removeClass("open-Menu")
  $('.menu-item .sub-menu').removeClass("open").slideUp().css("opacity", "");
});
$("header .menu > .menu-item > a").click(function(e){
  if($(window).width() <= 992){
    e.preventDefault();
    if ($(this).parent().children(".sub-menu").hasClass('open')){
      $(this).parent().removeClass("open-Menu");
      $(this).parent().children(".sub-menu").removeClass("open").slideToggle();
    }else if ($('.menu-item .sub-menu').hasClass('open')){
      $('.menu-item').removeClass("open-Menu");
      $('.menu-item .sub-menu').removeClass("open").slideUp();
      $(this).parent().children(".sub-menu").addClass("open").slideToggle();
      $(this).parent().addClass("open-Menu");
    }else{
      $(this).parent().addClass("open-Menu");
      $(this).parent().children(".sub-menu").addClass("open").slideToggle();
    }   
  }
});
$("header .menu > .menu-item").hover(function(e){
  e.preventDefault();
  e.stopPropagation()
  if($(window).width() > 992){
    if ($(this).children(".sub-menu").hasClass('open')){
        $(this).removeClass("open-Menu");
        $(this).children(".sub-menu").removeClass("open").slideUp().css("opacity","0");
    }else if ($('.menu-item .sub-menu').hasClass('open')){
      $(this).removeClass("open-Menu");
      $('.menu-item .sub-menu').removeClass("open").slideUp().css("opacity","0");
      $(this).children(".sub-menu").addClass("open").slideToggle('medium', function(){
        $(this).fadeTo(250, 1)
      });
    }else{
      $(this).addClass("open-Menu");
      $(this).children(".sub-menu").addClass("open").slideToggle('medium', function() {
        if ($(this).is(':visible'))
          $(this).css('display','inline-grid');

        $(this).fadeTo(250, 1)
      });

    } 
  }
});

</script>
<script src="<?php bloginfo('template_directory'); ?>/js/custom.js"></script>
<?php wp_footer(); ?>


</body>
</html>
