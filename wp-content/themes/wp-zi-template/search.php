<?php get_header(); ?>
	<div class="container searchResults">

		<?php
		$search_term = get_search_query();
		$search_args = array(
			'post_type' => array('post', 'page'),
            'order' => 'ASC',
            'posts_per_page' => -1,
			's' => $search_term);
			$search_query = new WP_Query($search_args);
		if ($search_query->have_posts()):
			while ($search_query->have_posts()):$search_query->the_post();
			$acfexcerpt = get_field("textContent"); ?>
			<div class="resultItem">
 				<h1 class="singleTitle"><a href="<?= get_permalink()?>"><?php the_title(); ?></a></h1>
				<h2 class="subTitle"><?=get_field('subtitulo'); ?></h2>
				<div class="resultContent">
					<?php 
						if (!empty(the_excerpt())): 
							the_excerpt(); 
						elseif (!empty($acfexcerpt)): 
							$acfexcerpt = get_field("textContent"); 
							echo substr($acfexcerpt, 0, 500).'[…]'; 
						endif;
					?>
				</div>
 			</div>

		 <?php
			endwhile;
		else:?>
			<h1 class="unknownSearch">Não foi encontrado nenhum artigo para a sua pesquisa.</h1>
		 <?php
	 		endif;
			?>


	</div>
<?php get_footer(); ?>
