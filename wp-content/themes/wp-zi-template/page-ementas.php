<?php get_header();?>
<?php
	$dia = date('w');
	$inicio_semana = date('Y/m/d', strtotime('-'.$dia.' days'));
	$fim_semana = date('Y/m/d', strtotime('+'.(6-$dia).' days'));
	//echo $fim_semana;
				$args = array(
						    'post_type' => 'ementa',
						    'posts_per_page' => -1,
						    'meta_key' => 'beginDate',
						    'meta_value_num' => $inicio_semana,
						    'meta_compare' => '>=',
						    'order' => 'ASC',
						    'meta_query' => array(
						        'relation' => 'AND',
						              array(
						            'key' => 'endDate',
						            'value' => $fim_semana,
						            'compare' => '<=',
						            'type' => 'date',
						        )
						   )
					);
	$postslist = get_posts( $args );

	if ($postslist){
		foreach ($postslist as $post) :  setup_postdata($post);
			$tdate = date('Y/m/d');
			$begin = get_field('beginDate');
			$end = get_field('endDate');
			$begindWeek = date('Y/m/d', strtotime($begin. ' - 1 days'));
			$endWeek = date('Y/m/d', strtotime($end. ' + 1 days'));
			if (($tdate >= $begindWeek) && ($tdate <= $endWeek)){
				$dayBegin = DateTime::createFromFormat("Y/m/d", $begin);
				$dateEnd = DateTime::createFromFormat("Y/m/d", $end);
				$dayDatebegin = $dayBegin->format('d');
				$dayDateend = $dateEnd->format('d');
				$monthDateend = $dateEnd->format('m');

				//likns-ementa
				$ementaBebes = get_field('ementaSalabebes');
				$ementa1_2Ano = get_field('ementaSala1-2ano');
				$ementaPre1_2Cilo = get_field('ementaPre1_2ciclos');
				switch ($monthDateend) {
					case "01":
							$monthDateend = "Janeiro";
							break;
					case "02":
							$monthDateend = "Fevereiro";
							break;
					case "03":
							$monthDateend = "Março";
							break;
					case "04":
							$monthDateend = "Abril";
							break;
					case "05":
							$monthDateend = "Maio";
							break;
					case "06":
							$monthDateend = "Junho";
							break;
					case "07":
							$monthDateend = "Julho";
							break;
					case "08":
							$monthDateend = "Agosto";
							break;
					case "09":
							$monthDateend = "Setembro";
							break;
					case "10":
							$monthDateend = "Outubro";
							break;
					case "11":
							$monthDateend = "Novembro";
							break;
					case "12":
							$monthDateend = "Dezembro";
							break;
					}
			}
		endforeach;
		if(empty($ementaBebes)){
			$ementaArray = "empty";
		}
	}

?>
<section class="main">
	<div class="container ementas">
		<h1>Ementas <span><?php if (empty($ementaArray)):echo $dayDatebegin.'-'.$dayDateend.' '.$monthDateend; endif;?></span></h1>
		<div class="ementaSubtitle">
			<p><?php if (empty($ementaArray)):echo "Descarregue a ementa ou veja online";else:echo "Disponível brevemente.";endif;?></p>
		</div>
		<?php if (empty($ementaArray)):?>
			<div class="row ementaItems m-0">
			<div class="col-12 col-sm-12 col-md-4 itemButton">
				<a class="buttonLink" href="<?= $ementaBebes;?>" title="Ver Ementa" target="_blank">
					<div class="topIcon"><i class="fas fa-download"></i></div>
					<div class="itemImg"><i class="fas fa-baby"></i></div>
					<div class="itemName"><p>Sala dos Bebés</p></div>
				</a>
			</div>
			<div class="col-12 col-sm-12 col-md-4 itemButton">
				<a class="buttonLink" href="<?=$ementa1_2Ano;?>" title="Ver Ementa">
					<div class="topIcon"><i class="fas fa-download"></i></div>
					<div class="itemImg"><i class="fas fa-child"></i></div>
					<div class="itemName"><p>Sala 1º e 2º ano</p></div>
				</a>
			</div>
			<div class="col-12 col-sm-12 col-md-4 itemButton">
				<a class="buttonLink" href="<?=$ementaPre1_2Cilo;?>" title="Ver Ementa">
					<div class="topIcon"><i class="fas fa-download"></i></div>
					<div class="itemImg"><i class="fas fa-child"></i></div>
					<div class="itemName"><p>Pré-Escolar 1º/2º Ciclos</p></div>
				</a>
			</div>
		</div>
		<?php endif; ?>
	</div>
</section>

<?php get_footer();?>


<script>
jQuery(document).ready(function(){
	jQuery('.scroller').scrollbar();
});
</script>
