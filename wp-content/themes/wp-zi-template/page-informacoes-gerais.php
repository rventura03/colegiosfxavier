<?php get_header();?>

<section class="main">
	<div class="container informacoesGerais">
		<h1>Informações Gerais</h1>
		<div class="contentInformacoesgerais">
			<?= the_field("textContent");?>
		</div>
	</div>
</section>

<?php get_footer();?>
