<?php get_header();?>

<section class="main">
	<div class="container ofertaFormativa">
		<h2 class="titleofertaFormativa">Oferta Formativa</h2>
		<div class="subtitle">
			<?=wpautop( $post->post_content );?>
		</div>
		<div class="schoolTabs">
			<div class="navigationMenutabs d-block d-md-none">
				<button id="prevTab" class="btnav" style="display: none;"><i class="fas fa-arrow-left"></i></button>
				<button id="nextTab" class="btnav"><i class="fas fa-arrow-right"></i></button>
			</div>
			<ul class="menuTabs">
				<li id="linkTab1" class="itemTab active"><a class="linkTab" href="#" title="Creche">Creche</a></li>
				<li id="linkTab2" class="itemTab"><a class="linkTab" href="#" title="Pré-Escolar">Pré-Escolar</a></li>
				<li id="linkTab3" class="itemTab"><a class="linkTab" href="#" title="1º Ciclo">1º Ciclo</a></li>
				<li id="linkTab4" class="itemTab"><a class="linkTab" href="#" title="2º Ciclo">2º Ciclo</a></li>
				<li id="linkTab5" class="itemTab"><a class="linkTab" href="#" title="Atividades de Complemento Curricular">Atividades de Compl. Curricular</a></li>
			</ul>
		</div>
	</div>
	<div class="container-fluid contentofertaFormativa">
		<div id="contentSlide" class="container">
				<div id="item1" class="itemSection linkTab1" data-slide="1" style="display: block;">
					<div class="row sliderofertaFormativa">
						<div class="col-12 col-sm-12 col-md-12 col-lg-4 itemTitle" syle="border: 1px solid;">
							<p>Creche</p>
						</div>
						<div class="col-12 col-sm-12 col-md-12 col-lg-8 itemImages" syle="border: 1px solid;">
								<?php if( have_rows('creche') ): ?>
									<div class="itemSlider" id="itemSlider">
								<?php while( have_rows('creche') ): the_row(); ?>
										<div class="item" data-img="<?php the_sub_field('creche_imgs'); ?>">
											<img src="<?php the_sub_field('creche_imgs'); ?>" alt="trans" width="310" height="310">
											<div class="show-icon">
												<i class="fas fa-search-plus"></i>
											</div>
										</div>
								<?php endwhile; ?>
								<?php endif; ?>
							</div>
						</div>
					</div>
					<div class="textofertaFormativa">
						<?=the_field("creche_texto");?>
					</div>
					<?php 
						if (have_rows('creche_docCat')):
					?>
					<div class="filesofertaFormativa">
						<div class="row m-0">
							<div class="col-12 col-sm-12 col-md-12 col-lg-4 filesofertaformativaTitle">
								<p class="filesTitle">Descarregue os ficheiros PDF aqui</p>
							</div>
							<div class="col-12 col-sm-12 col-md-12 col-lg-8 filesofertaformativaButtons">
								<div class="fileSlider">
									<?php if( have_rows('creche_docCat') ):
										?>
										<?php
										$i = "1";
										while( have_rows('creche_docCat') ): the_row();

										$title = get_sub_field('catTitle');
										$icon = get_sub_field('catImg');
										$document = get_sub_field('catFiles');
											echo '
											<div class="slick-slide row buttonFiles m-0">
												<div class="col itemFile" style="background-color:';
												if($i == "1"){
													echo '#1b2376;">';
												}else if($i == "2"){
													echo '#4169d6;">';
												}else if($i == "3"){
													echo '#51b5ee;">';
												}
												echo'<span class="topIcon"><i class="fas fa-download"></i></span>
													<span class="fileIcon"><img src="'.$icon.'" alt="Icon do Documento"/></span>
													<div class="filesList" style="display: none;">
														<div class="fListscroll">';
															while( have_rows('catFiles') ): the_row();
																$fileTitle = get_sub_field('titleFile');
																$fileUrl = get_sub_field('docFile');
																echo '<a href="'. $fileUrl .'" target="_blank"><p><span><i class="far fa-file-alt"></i></span>'. $fileTitle .'</p></a>';
															endwhile;	
													echo'</div></div>
													<p class="fileTitle">'.$title.'</p>
													<div class="arrowDown'.$i.'"></div>
												</div>
											</div>';
											if ($i != "3"){
												$i++;
											}else{
												$i = "1";
											}
										?>
										<?php endwhile;?>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
					<?php 
						endif;
					?>
				</div>

				<div id="item2" class="itemSection linkTab2" data-slide="2" style="display: none;">
					<div class="row sliderofertaFormativa">
						<div class="col-12 col-sm-12 col-md-12 col-lg-4 itemTitle" syle="border: 1px solid;">
							<p>Pré-Escolar</p>
						</div>
						<div class="col-12 col-sm-12 col-md-12 col-lg-8 itemImages" syle="border: 1px solid;">
								<?php if( have_rows('pre_escolar') ): ?>
									<div class="itemSlider" id="itemSlider">
								<?php while( have_rows('pre_escolar') ): the_row(); ?>
										<div class="item" data-img="<?php the_sub_field('preescolar_imgs'); ?>">
											<img src="<?php the_sub_field('preescolar_imgs'); ?>" alt="trans" width="310" height="310">
											<div class="show-icon">
												<i class="fas fa-search-plus"></i>
											</div>
										</div>
								<?php endwhile; ?>
								<?php endif; ?>
							</div>
						</div>
					</div>
					<div class="textofertaFormativa">
						<?=the_field("prescolar_texto");?>
					</div>
					<?php 
						if (have_rows('preescolar_docCat')):
					?>
					<div class="filesofertaFormativa">
						<div class="row m-0">
							<div class="col-12 col-sm-12 col-md-12 col-lg-4 filesofertaformativaTitle">
								<p class="filesTitle">Descarregue os ficheiros PDF aqui</p>
							</div>
							<div class="col-12 col-sm-12 col-md-12 col-lg-8 filesofertaformativaButtons">
								<div class="fileSlider">
									<?php if( have_rows('preescolar_docCat') ):
										?>
										<?php
										$i = "1";
										while( have_rows('preescolar_docCat') ): the_row();

										$title = get_sub_field('catTitle');
										$icon = get_sub_field('catImg');
										$document = get_sub_field('catFiles');
											echo '
											<div class="slick-slide row buttonFiles m-0">
												<div class="col itemFile" style="background-color:';
												if($i == "1"){
													echo '#1b2376;">';
												}else if($i == "2"){
													echo '#4169d6;">';
												}else if($i == "3"){
													echo '#51b5ee;">';
												}
												echo'<span class="topIcon"><i class="fas fa-download"></i></span>
													<span class="fileIcon"><img src="'.$icon.'" alt="Icon do Documento"/></span>
													<div class="filesList" style="display: none;">
														<div class="fListscroll">';
															while( have_rows('catFiles') ): the_row();
																$fileTitle = get_sub_field('titleFile');
																$fileUrl = get_sub_field('docFile');
																echo '<a href="'. $fileUrl .'" target="_blank"><p><span><i class="far fa-file-alt"></i></span>'. $fileTitle .'</p></a>';
															endwhile;	
													echo'</div></div>
													<p class="fileTitle">'.$title.'</p>
													<div class="arrowDown'.$i.'"></div>
												</div>
											</div>';
											if ($i != "3"){
												$i++;
											}else{
												$i = "1";
											}
										?>
										<?php endwhile;?>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
					<?php 
						endif;
					?>
				</div>

				<div id="item3" class="itemSection linkTab3" data-slide="3" style="display: none;">
					<div class="row sliderofertaFormativa">
						<div class="col-12 col-sm-12 col-md-12 col-lg-4 itemTitle" syle="border: 1px solid;">
							<p>1º Ciclo</p>
						</div>
						<div class="col-12 col-sm-12 col-md-12 col-lg-8 itemImages" syle="border: 1px solid;">
								<?php if( have_rows('1_ciclo') ): ?>
									<div class="itemSlider" id="itemSlider">
								<?php while( have_rows('1_ciclo') ): the_row(); ?>
										<div class="item" data-img="<?php the_sub_field('1ciclo_imgs'); ?>">
											<img src="<?php the_sub_field('1ciclo_imgs'); ?>" alt="trans" width="310" height="310">
											<div class="show-icon">
												<i class="fas fa-search-plus"></i>
											</div>
										</div>
								<?php endwhile; ?>
								<?php endif; ?>
							</div>
						</div>
					</div>
					<div class="textofertaFormativa">
						<?=the_field("1ciclo_texto");?>
					</div>
					<?php 
						if (have_rows('1ciclo_docCat')):
					?>
					<div class="filesofertaFormativa">
						<div class="row m-0">
							<div class="col-12 col-sm-12 col-md-12 col-lg-4 filesofertaformativaTitle">
								<p class="filesTitle">Descarregue os ficheiros PDF aqui</p>
							</div>
							<div class="col-12 col-sm-12 col-md-12 col-lg-8 filesofertaformativaButtons">
								<div class="fileSlider">
									<?php if( have_rows('1ciclo_docCat') ):
										?>
										<?php
										$i = "1";
										while( have_rows('1ciclo_docCat') ): the_row();

										$title = get_sub_field('catTitle');
										$icon = get_sub_field('catImg');
										$document = get_sub_field('catFiles');
											echo '
											<div class="slick-slide row buttonFiles m-0">
												<div class="col itemFile" style="background-color:';
												if($i == "1"){
													echo '#1b2376;">';
												}else if($i == "2"){
													echo '#4169d6;">';
												}else if($i == "3"){
													echo '#51b5ee;">';
												}
												echo'<span class="topIcon"><i class="fas fa-download"></i></span>
													<span class="fileIcon"><img src="'.$icon.'" alt="Icon do Documento"/></span>
													<div class="filesList" style="display: none;">
														<div class="fListscroll">';
															while( have_rows('catFiles') ): the_row();
																$fileTitle = get_sub_field('titleFile');
																$fileUrl = get_sub_field('docFile');
																echo '<a href="'. $fileUrl .'" target="_blank"><p><span><i class="far fa-file-alt"></i></span>'. $fileTitle .'</p></a>';
															endwhile;	
													echo'</div></div>
													<p class="fileTitle">'.$title.'</p>
													<div class="arrowDown'.$i.'"></div>
												</div>
											</div>';
											if ($i != "3"){
												$i++;
											}else{
												$i = "1";
											}
										?>
										<?php endwhile;?>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
					<?php 
						endif;
					?>
				</div>

				<div id="item4" class="itemSection linkTab4" data-slide="4" style="display: none;">
					<div class="row sliderofertaFormativa">
						<div class="col-12 col-sm-12 col-md-12 col-lg-4 itemTitle" syle="border: 1px solid;">
							<p>2º Ciclo</p>
						</div>
						<div class="col-12 col-sm-12 col-md-12 col-lg-8 itemImages" syle="border: 1px solid;">
								<?php if( have_rows('2_ciclo') ): ?>
									<div class="itemSlider" id="itemSlider">
								<?php while( have_rows('2_ciclo') ): the_row(); ?>
										<div class="item" data-img="<?php the_sub_field('2ciclo_imgs'); ?>">
											<img src="<?php the_sub_field('2ciclo_imgs'); ?>" alt="trans" width="310" height="310">
											<div class="show-icon">
												<i class="fas fa-search-plus"></i>
											</div>
										</div>
								<?php endwhile; ?>
								<?php endif; ?>
							</div>
						</div>
					</div>
					<div class="textofertaFormativa">
						<?=the_field("2ciclo_texto");?>
					</div>
					<?php 
						if (have_rows('2ciclo_docCat')):
					?>
					<div class="filesofertaFormativa">
						<div class="row m-0">
							<div class="col-12 col-sm-12 col-md-12 col-lg-4 filesofertaformativaTitle">
								<p class="filesTitle">Descarregue os ficheiros PDF aqui</p>
							</div>
							<div class="col-12 col-sm-12 col-md-12 col-lg-8 filesofertaformativaButtons">
								<div class="fileSlider">
									<?php if( have_rows('2ciclo_docCat') ):
										?>
										<?php
										$i = "1";
										while( have_rows('2ciclo_docCat') ): the_row();

										$title = get_sub_field('catTitle');
										$icon = get_sub_field('catImg');
										$document = get_sub_field('catFiles');
											echo '
											<div class="slick-slide row buttonFiles m-0">
												<div class="col itemFile" style="background-color:';
												if($i == "1"){
													echo '#1b2376;">';
												}else if($i == "2"){
													echo '#4169d6;">';
												}else if($i == "3"){
													echo '#51b5ee;">';
												}
												echo'<span class="topIcon"><i class="fas fa-download"></i></span>
													<span class="fileIcon"><img src="'.$icon.'" alt="Icon do Documento"/></span>
													<div class="filesList" style="display: none;">
														<div class="fListscroll">';
															while( have_rows('catFiles') ): the_row();
																$fileTitle = get_sub_field('titleFile');
																$fileUrl = get_sub_field('docFile');
																echo '<a href="'. $fileUrl .'" target="_blank"><p><span><i class="far fa-file-alt"></i></span>'. $fileTitle .'</p></a>';
															endwhile;	
													echo'</div></div>
													<p class="fileTitle">'.$title.'</p>
													<div class="arrowDown'.$i.'"></div>
												</div>
											</div>';
											if ($i != "3"){
												$i++;
											}else{
												$i = "1";
											}
										?>
										<?php endwhile;?>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
					<?php 
						endif;
					?>
				</div>

				<div id="item5" class="itemSection linkTab5" data-slide="5" style="display: none;">
					<div class="row sliderofertaFormativa">
						<div class="col-12 col-sm-12 col-md-12 col-lg-4 itemTitle" syle="border: 1px solid;">
							<p>Atividades de Complemento Curricular</p>
						</div>
						<div class="col-12 col-sm-12 col-md-12 col-lg-8 itemImages" syle="border: 1px solid;">
							<?php if( have_rows('imgs_acc') ): ?>
								<div class="itemSlider" id="itemSlider">
								<?php while( have_rows('imgs_acc') ): the_row(); ?>
									<div class="item" data-img="<?php the_sub_field('aac_imgs'); ?>">
										<img src="<?php the_sub_field('aac_imgs'); ?>" alt="trans" width="310" height="310">
										<div class="show-icon">
											<i class="fas fa-search-plus"></i>
										</div>
									</div>
								<?php endwhile; ?>
								</div>
							<?php endif; ?>
						</div>
					</div>
					<div class="textofertaFormativa">
						<?=the_field("aacText");?>
					</div>
					<?php 
						if (have_rows('aac_docCat')):
					?>
					<div class="filesofertaFormativa">
						<div class="row m-0">
							<div class="col-12 col-sm-12 col-md-12 col-lg-4 filesofertaformativaTitle">
								<p class="filesTitle">Descarregue os ficheiros PDF aqui</p>
							</div>
							<div class="col-12 col-sm-12 col-md-12 col-lg-8 filesofertaformativaButtons">
								<div class="fileSlider">
									<?php if( have_rows('aac_docCat') ):
										?>
										<?php
										$i = "1";
										while( have_rows('aac_docCat') ): the_row();

										$title = get_sub_field('catTitle');
										$icon = get_sub_field('catImg');
										$document = get_sub_field('catFiles');
											echo '
											<div class="slick-slide row buttonFiles m-0">
												<div class="col itemFile" style="background-color:';
												if($i == "1"){
													echo '#1b2376;">';
												}else if($i == "2"){
													echo '#4169d6;">';
												}else if($i == "3"){
													echo '#51b5ee;">';
												}
												echo'<span class="topIcon"><i class="fas fa-download"></i></span>
													<span class="fileIcon"><img src="'.$icon.'" alt="Icon do Documento"/></span>
													<div class="filesList" style="display: none;">
														<div class="fListscroll">';
															while( have_rows('catFiles') ): the_row();
																$fileTitle = get_sub_field('titleFile');
																$fileUrl = get_sub_field('docFile');
																echo '<a href="'. $fileUrl .'" target="_blank"><p><span><i class="far fa-file-alt"></i></span>'. $fileTitle .'</p></a>';
															endwhile;	
													echo'</div></div>
													<p class="fileTitle">'.$title.'</p>
													<div class="arrowDown'.$i.'"></div>
												</div>
											</div>';
											if ($i != "3"){
												$i++;
											}else{
												$i = "1";
											}
										?>
										<?php endwhile;?>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
					<?php 
						endif;
					?>
				</div>
		</div>
	</div>
	</div>
</section>

<script>
//List Files by category
$(".buttonFiles").click (function(e){
	e.stopPropagation();
	$(".buttonFiles .filesList").fadeOut();
	$(".buttonFiles .fileIcon").fadeIn();
	var idthis = $(this).attr("data-slick-index");
	console.log('.buttonFiles[data-slick-index="' + idthis + '"]');
	if ($('.buttonFiles[data-slick-index="' + idthis + '"] .filesList').css("display") == "block"){
		$(".buttonFiles .filesList").fadeOut();
		$(".buttonFiles .fileIcon").fadeIn();
	}else{
		$('.buttonFiles[data-slick-index="' + idthis + '"] .filesList').fadeIn();
		$('.buttonFiles[data-slick-index="' + idthis + '"] .fileIcon').fadeOut();
	}
});
$(window).click (function(){
	$(".buttonFiles .filesList").fadeOut();
	$(".buttonFiles .fileIcon").fadeIn();
});

//mobile button navigation for menu tabs
$(".btnav").click(function(e){
	e.stopPropagation();
	e.preventDefault();
	var current=0,
			next=$(this).attr('id');

	$('.itemSection').each(function( index ) {
		if ($(this).css("display")=='block'){
			current=$(this).data('slide');
			$(this).css({"display":"none"});
		}
	});

	$('#linkTab'+current).removeClass('active');
	if (next=='prevTab')
			current--;
	else
		current++;

	if (current <=1){
		$('#prevTab').hide();
		current=1;
	}else if (current >=$('.itemSection').length){
		current=$('.itemSection').length;
		$('#nextTab').hide();

	}else{
		$('#nextTab').show();
		$('#prevTab').show();
	}

	$('#item'+current).css({'display':'block'});
	$('#linkTab'+current).addClass('active');

	//restart slickslider
	$('.itemSlider').slick('unslick');
	$('.fileSlider').slick('unslick');
	$('.itemSlider').slick({
		autoplay: false,
		dots: true,
		arrows: true,
		slidesToShow: 3,
		slidesToScroll: 3,
		responsive: [
		{
		breakpoint: 768,
		settings: {
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: false
		}
		}],
	});
	$('.fileSlider').slick({
		autoplay: false,
		dots: true,
		arrows: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		infinite: false,
		responsive: [
		{
		breakpoint: 768,
		settings: {
			slidesToShow: 2,
			slidesToScroll: 1,
			dots: false
		}
		},
		{
		breakpoint: 365,
		settings: {
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: false
		}
		}]

	});

});

//menus Tabs
$(".itemTab").click(function(e){
	e.preventDefault();
	var id ='.'+$(this).attr('id');

	$('.itemTab').each(function( index ) {
	  $(this).removeClass('active');
	});

	$('.itemSection').each(function( index ) {
	  $(this).hide();
	});


	$(this).addClass('active');
	$(id).fadeIn();

	//restart slickslider
	$('.itemSlider').slick('unslick');
	$('.fileSlider').slick('unslick');
	$('.itemSlider').slick({
		autoplay: false,
		dots: true,
		arrows: true,
		slidesToShow: 3,
		slidesToScroll: 3,
		responsive: [
		{
		breakpoint: 768,
		settings: {
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: false
		}
		}],
	});
	$('.fileSlider').slick({
		autoplay: false,
		dots: true,
		arrows: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		infinite: false,
		responsive: [
		{
		breakpoint: 768,
		settings: {
			slidesToShow: 2,
			slidesToScroll: 1,
			dots: false
		}
		},
		{
		breakpoint: 365,
		settings: {
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: false
		}
		}]

	});
});

$('.itemSlider').slick({
	autoplay: false,
	dots: true,
	arrows: true,
	slidesToShow: 3,
	slidesToScroll: 3,
	responsive: [
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
		slidesToScroll: 1,
		dots: false
	  }
    }],
});
$('.itemSlider').slickLightbox({
  src: 'data-img',
  itemSelector: '.item'
});

$('.fileSlider').slick({
	autoplay: false,
	dots: true,
	arrows: true,
	slidesToShow: 3,
	slidesToScroll: 1,
	infinite: false,
	responsive: [
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
		slidesToScroll: 1,
		dots: false
      }
	},
	{
      breakpoint: 365,
      settings: {
        slidesToShow: 1,
		slidesToScroll: 1,
		dots: false
      }
    }]
});
</script>



<?php get_footer();?>
