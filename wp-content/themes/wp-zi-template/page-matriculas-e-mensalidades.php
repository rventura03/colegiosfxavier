<?php get_header();?>

<section class="main">
	<div class="container matriculasMensalidades">
		<h1>Ano Letivo <span><?php the_field('yearSchool')?></span></h1>
		<div class="subscAnual">
			<p>Inscrição Anual | Matricula | <span><?php the_field('priceAnualSubs')?></span><span class="subscIcon"><i class="fas fa-medal"></i></span></p>
		</div>
		<div class="monthCosts">
			<p class="titleMonthcosts">Mensalidades</p>
			<div class="outerWrap">
				<div class="tableWrap">
					<table class="tableMonthcosts">
						<thead>
							<tr>
								<th class="thead1">Mensalidade</th>
								<th class="thead2">1 Aluno</th>
								<th class="thead3">2º Irmão</th>
								<th class="thead4">3º Irmão</th>
							</tr>
						</thead>
						<tbody>
							<tr class="spacer">
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<?php if( have_rows('mensalidades') ): ?>
								<?php while( have_rows('mensalidades') ): the_row(); ?>
									<?php if( have_rows('monthlyCreche') ): ?>
										<?php while( have_rows('monthlyCreche') ): the_row(); ?>
											<tr>
												<th class="sideThead">Creche</th>
												<td><?= get_sub_field('creche1Aluno');?></td>
												<td><?= get_sub_field('creche2Irmao');?></td>
												<td><?= get_sub_field('creche3Irmao');?></td>
											</tr>
										<?php endwhile; ?>
									<?php endif; ?>
									<?php if( have_rows('monthlyPreescolar') ): ?>
										<?php while( have_rows('monthlyPreescolar') ): the_row(); ?>
											<tr>
												<th class="sideThead">Pré-Escolar</th>
												<td><?= get_sub_field('preescolar1Aluno');?></td>
												<td><?= get_sub_field('preescolar2Irmao');?></td>
												<td><?= get_sub_field('preescolar3Irmao');?></td>
											</tr>
										<?php endwhile; ?>
									<?php endif; ?>
									<?php if( have_rows('monthly1Ciclo') ): ?>
										<?php while( have_rows('monthly1Ciclo') ): the_row(); ?>
											<tr>
												<th class="sideThead">1º Ciclo</th>
												<td><?= get_sub_field('1ciclo1Aluno');?></td>
												<td><?= get_sub_field('1ciclo2Irmao');?></td>
												<td><?= get_sub_field('1ciclo3Irmao');?></td>
											</tr>
										<?php endwhile; ?>
									<?php endif; ?>
									<?php if( have_rows('monthly2Ciclo') ): ?>
										<?php while( have_rows('monthly2Ciclo') ): the_row(); ?>
											<tr>
												<th class="sideThead">2º Ciclo</th>
												<td><?php the_sub_field('2ciclo1Aluno');?></td>
												<td><?php the_sub_field('2ciclo2Irmao');?></td>
												<td><?php the_sub_field('2ciclo3Irmao');?></td>
											</tr>
										<?php endwhile; ?>
									<?php endif; ?>
								<?php endwhile; ?>
							<?php endif; ?>
						</tbody>
					</table>
				</div>
			</div>
			<?php if( have_rows('mensalidades') ): ?>
				<?php while( have_rows('mensalidades') ): the_row(); ?>
					<?php if( have_rows('monthlyNotes') ): ?>
						<?php while( have_rows('monthlyNotes') ): the_row(); ?>
							<p class="infoField"><?php the_sub_field('paragrafo');?></p>
						<?php endwhile; ?>
					<?php endif; ?>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
		<div class="extendCosts">
			<p class="titleExtendcosts">Prolongamentos</p>
			<div class="extendWrapper">
				<div class="innerWrapper">
					<div class="extendcostsPreescolar">
						<p class="subtitleExtendcosts">Pré-Escolar</p>
						<table class="tableExtendcosts">
							<thead>
								<tr>
									<th class="thead1">Prolongamento</th>
									<th class="thead2">Mensais</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
								<?php if( have_rows('prolongamentos') ): ?>
									<?php while( have_rows('prolongamentos') ): the_row(); ?>
										<?php if( have_rows('extendPreescolar') ): ?>
											<?php while( have_rows('extendPreescolar') ): the_row(); ?>
												<tr>
													<td class="highHead"><?php the_sub_field('extendHours');?></td>
													<td><?php the_sub_field('extendMonthlyprice');?></td>
												</tr>
											<?php endwhile; ?>
										<?php endif; ?>
									<?php endwhile; ?>
								<?php endif; ?>
							</tbody>
						</table>
					</div>
					<div class="extendcosts1Ciclo">
						<p class="subtitleExtendcosts">1º Ciclo</p>
						<table class="tableExtendcosts">
							<thead>
								<tr>
									<th class="thead1">Prolongamento</th>
									<th class="thead2">Mensais</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
								<?php if( have_rows('prolongamentos') ): ?>
									<?php while( have_rows('prolongamentos') ): the_row(); ?>
										<?php if( have_rows('extend1Ciclo') ): ?>
											<?php while( have_rows('extend1Ciclo') ): the_row(); ?>
												<tr>
													<td class="highHead"><?php the_sub_field('extendHours');?></td>
													<td><?php the_sub_field('extendMonthlyprice');?></td>
												</tr>
											<?php endwhile; ?>
										<?php endif; ?>
									<?php endwhile; ?>
								<?php endif; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<?php if( have_rows('prolongamentos') ): ?>
				<?php while( have_rows('prolongamentos') ): the_row(); ?>
					<?php if( have_rows('extendNotas') ): ?>
						<?php while( have_rows('extendNotas') ): the_row(); ?>
							<p class="infoField"><?php the_sub_field('paragrafo');?></p>
						<?php endwhile; ?>
					<?php endif; ?>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
		<div class="mealsCosts">
			<p class="titleMealscosts">Refeições</p>
			<div class="outerWrap">
				<div class="tableWrap">
					<table class="tableMealscosts">
						<tbody>
							<tr class="spacer">
								<th class="thead1 d-table-cell d-sm-none" valign="center">Refeições</th>
								<th class="thead1 d-none d-sm-table-cell" valign="center" rowspan="3">Refeições</th>
								<th class="thead2">1 Aluno</th>
								<th class="thead3">1 Aluno</th>
								<th class="thead4" >2º Irmão 1º Ciclo</th>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td class="d-table-cell d-sm-none">&nbsp;</td>
								<td>Pré-Escolar</td>
								<td>1º Ciclo</td>
								<td>Pré-Escolar</td>
							</tr>
							<?php if( have_rows('refeicoes') ): ?>
								<?php while( have_rows('refeicoes') ): the_row(); ?>
									<?php if( have_rows('lunch') ): ?>
										<tr>
											<td class="highHead">Almoço</td>
										<?php while( have_rows('lunch') ): the_row(); ?>
											<td><?php the_sub_field('lunchPreescolar1Aluno');?></td>
											<td><?php the_sub_field('lunch1ciclo1Aluno');?></td>
											<td><?php the_sub_field('lunchPreescolar2Irmao1Ciclo');?></td>
										<?php endwhile; ?>
									</tr>
									<?php endif; ?>
									<?php if( have_rows('snack') ): ?>
										<tr>
											<td class="highHead">Lanche</td>
										<?php while( have_rows('snack') ): the_row(); ?>
											<td><?php the_sub_field('snackPreescolar1Aluno');?></td>
											<td><?php the_sub_field('snack1ciclo1Aluno');?></td>
											<td><?php the_sub_field('snackPreescolar2Irmao1Ciclo');?></td>
										<?php endwhile; ?>
									</tr>
									<?php endif; ?>
								<?php endwhile; ?>
							<?php endif; ?>
						</tbody>
					</table>
				</div>
			</div>
			<?php if( have_rows('refeicoes') ): ?>
				<?php while( have_rows('refeicoes') ): the_row(); ?>
					<?php if( have_rows('notas') ): ?>
						<?php while( have_rows('notas') ): the_row(); ?>
							<p class="infoField"><?php the_sub_field('paragrafo');?></p>
						<?php endwhile; ?>
					<?php endif; ?>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</div>
	<div class="container matriculasInfo">
		<?php if( have_rows('informations') ): ?>
			<?php while( have_rows('informations') ): the_row(); ?>
				<h1>Informações</h1>
				<?php if( have_rows('documents') ): ?>
					<div class="docsMatriculasinfo">
						<h3 class="subtitleDocsmatriculasinfo">Documentos a trazer</h3>
						<ul class="listDocsmatriculasinfo">
						<?php while( have_rows('documents') ): the_row(); ?>
							<li><i class="fas fa-check"></i><?php the_sub_field('itemDocument');?></li>
						<?php endwhile; ?>
						</ul>
					</div>
				<?php endif; ?>
				<?php if( have_rows('revovationDocuments') ): ?>
					<div class="renovMatriculasinfo">
						<h3 class="subtitleDocsmatriculasinfo">Renovação de matriculas</h3>
						<ol>
							<?php while( have_rows('revovationDocuments') ): the_row(); ?>
								<li><?php the_sub_field('itemRenovation');?></li>
							<?php endwhile; ?>
						</ol>
					</div>
				<?php endif; ?>
				<?php if( have_rows('monthlyInfo') ): ?>
					<div class="mensalidadesInfo">
						<h3 class="titleMensalidadesinfo">Mensalidades</h3>
						<h3 class="subtitleMensalidadesinfo">Prazo e Modalidade de Pagamento</h3>
						<ol>
							<?php while( have_rows('monthlyInfo') ): the_row(); ?>
								<li><?php the_sub_field('itemMonthly');?></li>
							<?php endwhile; ?>
						</ol>
					</div>
				<?php endif; ?>
			<?php endwhile; ?>
		<?php endif; ?>

	</div>

</section>

<?php get_footer();?>


<script>
$(document).ready(function() {
  $('tbody').scroll(function(e) { //detect a scroll event on the tbody
  	/*
    Setting the thead left value to the negative valule of tbody.scrollLeft will make it track the movement
    of the tbody element. Setting an elements left value to that of the tbody.scrollLeft left makes it maintain 			it's relative position at the left of the table.
    */
    $('thead').css("left", -$("tbody").scrollLeft()); //fix the thead relative to the body scrolling
    $('thead th:nth-child(1)').css("left", $("tbody").scrollLeft()); //fix the first cell of the header
    $('tbody td:nth-child(1)').css("left", $("tbody").scrollLeft()); //fix the first column of tdbody
  });
});

jQuery(document).ready(function(){
	jQuery('.scroller').scrollbar();
});
</script>
